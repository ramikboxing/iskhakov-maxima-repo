<html>
<#-- страница для логина -->
<head>
    <title>Login</title>
</head>
<body>
<#-- вызываем метод POST по адресу /login/process -->
<form action="/login/process" method="post">
    <div>
        Email: <input name="email" type="email">
    </div>
    <div>
        Password: <input name="password" type="password">
    </div>
    <input type="submit">
<#--    Проверяем в модели аттрибут error равен true-->
    <#if error??>
        <p> Пароль или логин неверны, попробуйте ещё раз </p>
    </#if>
</form>
</body>
</html>
