<html>
<head>
    <title>Sign Up</title>
</head>
<body>
<form action="/sign_up" method="post" modelAttribute="user">
    <div>
        <label path="name">Name</label>
        <input path="name" name="name"/>
        <errors path="name"/>
    </div>
    <div>
        <label path="surname">Surname</label>
        <input path="surname" name="surname"/>
        <errors path="surname"/>
    </div>
    <div>
        <label path="email">Email</label>
        <input path="email" type="email" name="email" placeholder=""/>
        <errors path="email"/>
    </div>
    <div>
        <label path="password">Password</label>
        <input path="password" type="password" name="password"/>
        <errors path="password"/>
    </div>
    <input type="submit">
</form>
</body>
</html>