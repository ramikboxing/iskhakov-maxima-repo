package ru.maxima.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.maxima.model.User;
import ru.maxima.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Component      // создаем бин authProvider для выполнения аутентиф (проверяет есть ли пользователь в базе данных)
public class AuthProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository repository;  // внедряем бин репозиториия

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();    // получаем ID пользователя в виде email (логин)
        User user = repository.findByEmail(email);  // находим пользователя по email (логин)
        if (user == null) {                         // проверяем, что пользователь с таким логином существует
            throw new UsernameNotFoundException("User not found");
        }
        String password = authentication.getCredentials().toString();   // достаём пароль из authentication
        if (!passwordEncoder.matches(password, user.getPassword())) { // и введенный пароль совпадает с паролем в базе
            throw new BadCredentialsException("Password is not correct");
        }
        List<GrantedAuthority> autorities = new ArrayList<>();
        return new UsernamePasswordAuthenticationToken(user, null, autorities); // возвращаем токен = пользователь
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
