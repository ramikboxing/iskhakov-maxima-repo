//package ru.maxima.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import ru.maxima.security.AuthProvider;
//
//@Configuration
//@EnableWebSecurity
//@ComponentScan("ru.maxima.security")
//public class SecurityConfig extends WebSecurityConfigurerAdapter {  // описываем разделение доступа
//
//    @Autowired
//    private AuthProvider authProvider;
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {  // вызывает authProvider который мы описали в классе AuthProvider
//        auth.authenticationProvider(authProvider);
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {      // пропускаем запрос через цепочку фильтров для разделение доступа
//
//        http
//                .authorizeRequests()
//                    .antMatchers("/sign_up", "/login").anonymous()   // адреса для не аутентифицированных пользователей
//                    .antMatchers("/users", "").authenticated()          // адреса для аутентифицированных пользователей
//
//                .and().csrf().disable()         // отключили проверку csrf аттак (отключили для удобства)
//                .formLogin() // указываем форму для логирования (аутентификации)
//                    .loginPage("/login")            //  указываем страницу для логирования (аутентификации)
//                    .loginProcessingUrl("/login/process")// вызываем метод POST по адресу /login/process
////                .passwordParameter("")
//                .failureUrl("login?error=true")
//                .usernameParameter("email")         // указываем, что вместо username Spring Security ищет параметр email (как логин)
////                    .defaultSuccessUrl("/users")        // указывает на какую страницу перейти в случае успешной аутентификации
//
//                .and().logout();
//    }
//
//    @Bean
//    public PasswordEncoder passwordEncoder(){
//        return new BCryptPasswordEncoder();
//    }
//}
