package ru.maxima.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

    /**
     * Класс аналогичный web.xml
     */
    public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer { // не ставим @Configuration

        @Override
        protected Class<?>[] getRootConfigClasses() {   // загружает основные конфигурации,
            // импортируем бины из других конфигов
            System.out.println("WebXML1");
            return new Class<?>[]{PersistenceConfig.class};
        }

        @Override
        protected Class<?>[] getServletConfigClasses() {    // описывает dispatcher servlet
            System.out.println("WebXML2");
            return new Class<?>[]{WebConfig.class};         // <servlet> tag in web.xml, возвращаем WebConfig
        }

        @Override
        protected String[] getServletMappings() {           // <servlet-mapping> tag in web.xml
            System.out.println("WebXML3");
            return new String[]{"/"};
        }
    }


