package ru.maxima.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Класс аналогичный persistence-config.xml
 */
@Configuration                                      // поскольку это конфигурационный файл
@PropertySource("classpath:persistence.properties") // подгружает проперти из persistence.properties
@EnableJpaRepositories(basePackages = "ru.maxima.repository")      // указываем где находится наш репозиторий
@EnableTransactionManagement                        // задаём транзакции через аннотации
public class PersistenceConfig {

    @Bean                                                                   // создаём бин dataSource
    public DataSource dataSource(
            @Value("${driver}") String driver,                              // читаем driver из persistence.properties
            @Value("${url}") String url,                                    // читаем url из persistence.properties
            @Value("${username}") String username,                          // читаем username из persistence.properties
            @Value("${password}") String password                           // читаем password из persistence.properties
    ) {
        System.out.println("dataSource");
        DriverManagerDataSource dataSource = new DriverManagerDataSource(); // создаём объект DriverManagerDataSource
        dataSource.setDriverClassName(driver);                              // указываем driver в dataSource
        dataSource.setUrl(url);                                             // указываем url в dataSource
        dataSource.setUsername(username);                                   // указываем username в dataSource
        dataSource.setPassword(password);                                   // указываем password в dataSource
        return dataSource;                                                  // возвращаем бин dataSource
    }

    @Bean                                                                   // создаём бин jdbcTemplate
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {               // c типом JdbcTemplate
        return new JdbcTemplate(dataSource);                                // возвращаем бин JdbcTemplate
    }

    @Bean                                                                       // создаём бин sessionFactory
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {      // c типом LocalSessionFactoryBean
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean(); // создаём объект LocalSessionFactoryBean
        sessionFactory.setDataSource(dataSource);                               // задаём dataSource в sessionFactory
        sessionFactory.setPackagesToScan("ru.maxima.model");                    // задаём пакет с моделями

        Properties properties = new Properties();
        properties.put("hibernate.show_sql", true);
        sessionFactory.setHibernateProperties(properties);

//        sessionFactory.setHibernateProperties(new Properties() {{
//            put("hibernate.show_sql", true);                                    // задаём свойства через ключ-значение
//        }});
        return sessionFactory;                                                  // возвращаем бин sessionFactory
    }

    @Bean                                                                               // создаём бин
    public EntityManagerFactory entityManagerFactory(DataSource dataSource) {            // тип EntityManagerFactory
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();   // создаём jpaVendorAdapter
        jpaVendorAdapter.setGenerateDdl(true);                                          // задаём свойства
        jpaVendorAdapter.setShowSql(true);                                              // задаём свойства
        LocalContainerEntityManagerFactoryBean entityManagerFactory =
                new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(dataSource);                                 // задаём dataSource
        entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter);                     // задаём jpaVendorAdapter
        entityManagerFactory.setPackagesToScan("ru.maxima.model");                      // задаём пакет с моделями
        entityManagerFactory.afterPropertiesSet();                                      // метод постинициализации бина
        return entityManagerFactory.getObject();                                        // вернуть собранный бин
    }

    @Bean                                                                               // создаём бин
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();         // создаём transactionManager
        transactionManager.setEntityManagerFactory(entityManagerFactory);               // задаём entityManagerFactory
        return transactionManager;                                                      // возвращаем transactionManager
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslator() {  // создаём бин
        return new PersistenceExceptionTranslationPostProcessor();                          // возвращаем бин
    }
}

