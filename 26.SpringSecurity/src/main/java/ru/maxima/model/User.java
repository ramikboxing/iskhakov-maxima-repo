package ru.maxima.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity // Сущность
@Table(name = "user_demo") // Таблица сущности в БД
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(name = "first_name")
    private String name;

    @NonNull
    private String last_name;

    @NonNull
    private String email;

    @NonNull
    private String password;    // для того, чтобы пользователя аутентифицировать, нужно добавить пароль
}









