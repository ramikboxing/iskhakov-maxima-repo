package ru.maxima.service;

import ru.maxima.model.User;

import java.util.List;

public interface UserService {  // методы такие же как и в UserDao, чтобы удобно было рефакторить контроллеры
    List<User> getAll();

    void add(User user);

    List<User> getByEmail(String email);
}
