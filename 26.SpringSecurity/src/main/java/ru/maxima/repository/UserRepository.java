package ru.maxima.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.model.User;

import java.util.List;

 //разновидность аннотации \@Component для репозиториев, создает бин userRepository
// создаем именнно интерфейс,реализацию не пишем,
// query method создает метод по названию,
// Отличие JpaRepository от CrudRepository
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
     List<User>findByEmailStartingWith(String prefix);
    List<User>findByEmailEndingWith(String suffix);
}
