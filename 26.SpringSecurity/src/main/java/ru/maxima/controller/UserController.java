package ru.maxima.controller;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.ui.Model;
import ru.maxima.dao.UserDao;
import ru.maxima.model.User;
import ru.maxima.service.UserService;

import java.util.List;

@Controller // создаём бин
public class UserController {

// @Qualifier("hibernateUserDao")  // поскольку у нас два бина типа userDao (hibernateUserDao и jpaUserDao),
// то указываем какой именно бин внедрять

    @Autowired
    private UserService service;

    @GetMapping("/")
    public String home(){
        return "redirect:/users";
    }


    @GetMapping("/users") // запрос возвращает страницу по адресу /users
    public String getUsers(Model model) {
        List<User> users = service.getAll();                // с помощью DAO мы возвращаем список пользователей
        for (User u:users ) {
            System.out.println(u.getName());
        }
        model.addAttribute("usersAttr", users); // с помощью Model users в аттрибут usersAttr
        return "/users";        // с помощью View мы указываем куда отправить Model (users.ftl)
    }   // отправляем ModelAndView

    @GetMapping("/users/new")   // возвращает страницу, заполнив которую мы вызовем метод POST
    public String getSignUp() {
        return "/sign_up";          // View для вызова метода POST
    }

    @PostMapping("/users/new")
    public String signUp(@ModelAttribute User user) {    // блягодаря @ModelAttribute мы создаем нового USER
        // с параметрами, которые указали в sign_up.ftl
        service.add(user);                          // добавляем в БД через DAO
        return "redirect:/users";           // повторно вызываем метод GET getUsers() по endpoint ("/users")
        // и возвращает новый список
    }
    @GetMapping("/search")
    public String getSearch(){
        return "/search";}

    @PostMapping("/search")
    public String search(Model model, String email){
        List<User>users = service.getByEmail(email);
        model.addAttribute("usersAttr", users);
        return "/users";
}
}

// /WEB-INF/templates/users.ftl

// Controller ==> Model (usersAttr) ==> View (/WEB-INF/templates/users.ftl)

