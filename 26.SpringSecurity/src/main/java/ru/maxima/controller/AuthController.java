package ru.maxima.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.maxima.model.User;
import ru.maxima.service.UserService;

@Controller                         // создаём бин
public class AuthController {       // создаём отдельный контроллер для аутентификации, авторизации, регистрации

    @Autowired                      // внедряем ранее созданный бин userService
    private UserService service;

    @GetMapping("/sign_up")    // возвращает страницу, заполнив которую мы вызовем метод POST
    public String getSignUp(Model model) {
        model.addAttribute("user",new User());
        return "/auth/sign_up";          // View для вызова метода POST
    }

    @PostMapping("/sign_up")                         // handler по регистрации
    public String signUp(@ModelAttribute User user) {   // благодаря @ModelAttribute мы создаем нового USER
                                                        // с параметрами, которые указали в sign_up.ftl
        service.add(user);                              // добавляем в БД через service
        return "redirect:/users";                       // повторно вызываем метод GET getUsers() по endpoint ("/users")
            // и возвращает новый список
    }

    @RequestMapping("/login")
    public String login(@RequestParam(value = "error",required = false) Boolean error,
                        Model model){
        if (Boolean.TRUE.equals(error)) {
            model.addAttribute("error", error);
        }
        return "/auth/sign_in";                          // возвращает страничку для логина или аутентификации
    }
}
