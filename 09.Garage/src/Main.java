import Garage.*;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Car audi = new Car("Audi", "A4");
        Car bmw = new Car("BMW", "X5");
        Car toyota = new Car("Toyota", "Camry");
        Car dodge = new Car("Dodge", "Viper");
        Car mercedes = new Car("Mercedes", "Maybach");
        Car opel = new Car("Opel", "Astra");

        Car[] carsGarage1 = {audi, bmw, toyota, dodge, mercedes, opel};
        Car[] carsGarage2 = new Car[carsGarage1.length];
        Driver driver = Driver.getInstance("Mike");
        Garage garage1 = new Garage("AmirhanaStreet", carsGarage1);
        Garage garage2 = new Garage("ShinikovStreet", carsGarage2);

        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("0. Печатать список машин в гаражах");
            System.out.println("1. Назначить водителя на машину");
            System.out.println("2. Пергнать машину");
            System.out.println("3. Перегнать все машины");
            System.out.println("9. Выход из программы");
            int select;
            try {
                select = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Ошибка ввода");
                continue;
            }
            if (select == 0) {
                System.out.println("Гараж на ул Амирхана");
                Garage.PrintCars(garage1);

                System.out.println("Гараж на ул Шинников");
                Garage.PrintCars(garage2);
                System.out.println("");
                continue;
            } else if (select == 1) {
                System.out.print("Выбирете номер машины из списка__");
                int numCar = scanner.nextInt();
                driver.setCarFoDriver(garage1.getCars()[numCar]);
                continue;
            } else if (select == 2) {
                System.out.print("Какую машину перегнать ?__");
                int numCar = scanner.nextInt();
                try {
                    garage1.getCars()[numCar].RunOver(garage1, garage2);
                } catch (NullPointerException e) {
                    System.out.println("Машины нет в указанном гараже");
                    continue;
                }
            } else if (select == 3) {
                for (int i = 0; i < garage1.getCars().length; i++){
                    driver.setCarFoDriver(garage1.getCars()[i]);
                    garage1.getCars()[i].RunOver(garage1, garage2);
                }
            } else if (select == 9) {
                break;
            }

        }

    }
}
