package Garage;

public class Driver {
    private static Driver instance;
    private String name;
    private Car car = null;

    private Driver(String name) {
        this.name = name;
    }

    public static Driver getInstance(String name) {
        if (instance == null) {
            instance = new Driver(name);
        }
        return instance;
    }

    public String getName() {
        return name;
    }

    public Car getCar() {
        return car;
    }

    public void setCarFoDriver(Car car) {
        car.setDriver(instance);
        this.car = car;
    }
}
