package Garage;

public class Garage {
    private String address;
    private Car cars[];

    public Garage(String address, Car[] cars) {
        this.address = address;
        this.cars = cars;
    }

    public String getAddress() {
        return address;
    }

    public void setCars(Car[] cars) {
        this.cars = cars;
    }

    public Car[] getCars() {
        return cars;
    }

    public static void PrintCars(Garage garage) {
        for (int i = 0; i < garage.getCars().length; i++) {
            System.out.print(i + "-");
            if (garage.getCars()[i] != null) {
                System.out.println(garage.getCars()[i].getBrand() + " " + garage.getCars()[i].getModel());
            } else {
                System.out.println("Свободное место");
            }
        }
    }
}
