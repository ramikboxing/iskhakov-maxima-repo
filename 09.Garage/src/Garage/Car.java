package Garage;

import java.util.Objects;

public class Car {
    private String brand;
    private String model;
    private Driver driver;

    public Car(String brand, String model) {
        this.brand = brand;
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public Driver getDriver() {
        return driver;
    }

    private void notDriver() {
        this.driver = null;
    }

    protected void setDriver(Driver driver) {
        if (driver.getCar() == null) {
            this.driver = driver;
            return;
        } else {
            driver.getCar().notDriver();
            this.driver = driver;
        }
    }

    public void RunOver(Garage a, Garage b) {
        if (this.driver != null) {
            for (int i = 0; i < a.getCars().length; i++) {
                if (this.equals(a.getCars()[i])) {
                    for (int j = 0; j < b.getCars().length; j++) {
                        if (b.getCars()[j] == null) {
                            b.getCars()[j] = a.getCars()[i];
                            a.getCars()[i] = null;
                            return;
                        }
                    }
                    System.out.println("Машины нет в гараже");
                    return;
                }
            }
        } else {
            System.out.println("У машины нет водителя");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return brand.equals(car.brand) && model.equals(car.model) && driver.equals(car.driver);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, driver);
    }
}
