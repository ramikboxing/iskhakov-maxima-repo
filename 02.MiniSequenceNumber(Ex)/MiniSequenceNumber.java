/*На вход программы последовательно поступают числа (общее количество чисел ограничено значением int, 
может быть и десять чисел, а может быть миллион). Последовательность заканчивается -1.
Нужно вывести число, которое встретилось меньшего всего раз
Условия:
Значение каждого числа последовательности находится в диапазоне -100 до 100
Последовательность ограничена значением int.
Пример последовательности:
100, 5, 2, 3, 3, 5, 100, -100, -100, -100, -1 (-1 не учитывается, означает конец последовательности)
Результат работы программы: 2 - встретилась 1 раз.*/
import java.util.Scanner;

class MiniSequenceNumber{
	
	public static void main(String[] args) {

		int [] array = new int[201];
		int miniSequenceNumber = Integer.MAX_VALUE;
		int minNumber = 0;
		System.out.println("Автозаполнение массива нажмите 0, ручное заполнение нажмите 1");
		Scanner scanner = new Scanner(System.in);
		int enter = 0;
		enter = scanner.nextInt();
		switch(enter){
			case 0: // автозаполнение
				while(true){
					int num = (int) (Math.random() * 200 - 100);
					if(num == -1){
						System.out.println("The end");
						break;
					}
					array[num + 100]+=1;		
				}
				break;
			case 1: // ручное заполнение	
				int num = 0;
				int count = 0;
				while(true){
					num = scanner.nextInt();
					if(num == -1){
						break;
					}
					array[num+100]++;
				}
				break;

		}//switch

		//test
		for (int i = 0; i < array.length; i++ ) {
			System.out.println(array[i]+"---- "+ i);
		}
		//test

		for (int i = 0; i < array.length; i++ ) {

			if(array[i] != 0 && miniSequenceNumber > array[i]){
				miniSequenceNumber = array[i];
				minNumber = i - 100;
			}	
		}		
	System.out.println("Число с минимальным количеством повторений: "+ minNumber + " встречается "+ miniSequenceNumber + " раз"  ); 		
	}
}