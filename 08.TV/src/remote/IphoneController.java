package remote;

import television.TV;

public class IphoneController implements TVRemote {
    private TV tv;

    public IphoneController(TV tv) {
        this.tv = tv;
    }

    @Override
    public void switchToChannel(int x) {
        System.out.println("iRemote");
        tv.showChannel(tv.searchChannel(x));
    }

    @Override
    public void nextChannel() {
        System.out.println("iRemote");
        tv.nextChannel();
    }

    @Override
    public void previousChannel() {
        System.out.println("iRemote");
        tv.previousChannel();
    }

    @Override
    public void lastChannel() {
        System.out.println("iRemote");
        tv.lastChannel();
    }

    @Override
    public void on() {
        System.out.println("iRemote");
        tv.on();
    }

    @Override
    public void off() {
        System.out.println("iRemote");
        tv.off();
    }
}
