package remote;

import television.TV;

public class RadioRemoteImpl implements TVRemote {

    private TV tv;

    public RadioRemoteImpl(TV tv) {
        this.tv = tv;
    }

    @Override
    public void switchToChannel(int x) {
        tv.showChannel(tv.searchChannel(x));
    }

    @Override
    public void nextChannel() {
        tv.nextChannel();
    }

    @Override
    public void previousChannel() {
        tv.previousChannel();
    }

    @Override
    public void lastChannel() {
        tv.lastChannel();
    }

    @Override
    public void on() {
        tv.on();
    }

    @Override
    public void off() {
        tv.off();
    }

}
