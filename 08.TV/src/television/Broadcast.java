package television;

public abstract class Broadcast {
    protected String broadcastName;
    protected int broadcastTimeHourStart;
    protected int broadcastTimeMinuteStart;
    protected int broadcastTimeHourEnd;
    protected int broadcastTimeMinuteEnd;

    public Broadcast(String broadcastName, int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                     int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        this.broadcastName = broadcastName;
        this.broadcastTimeHourStart = broadcastTimeHourStart;
        this.broadcastTimeMinuteStart = broadcastTimeMinuteStart;
        this.broadcastTimeHourEnd = broadcastTimeHourEnd;
        this.broadcastTimeMinuteEnd = broadcastTimeMinuteEnd;
    }
}
