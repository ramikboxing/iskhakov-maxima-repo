package television;

public class Advertising extends Broadcast{

    public Advertising( int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                        int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Реклама", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
