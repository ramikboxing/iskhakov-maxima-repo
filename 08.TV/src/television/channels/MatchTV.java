package television.channels;

import television.Broadcast;
import television.ChannelTV;
import television.broadcast.MatchTV.*;


public class MatchTV extends ChannelTV {
    private static News news = new News(6, 0, 8, 0);
    private static AllForTheMatch allForTheMatch = new AllForTheMatch(8, 15, 10, 30);
    private static News news2 = new News(10, 50, 12, 0);
    private static SpecialReport specialReport = new SpecialReport(12, 10, 13, 50);
    private static FighterWithoutRules fighterWithoutRules = new FighterWithoutRules(14, 0, 16, 0);
    private static ThereIsATheme thereIsATheme = new ThereIsATheme(16, 10, 18, 0);
    private static TheMainRoad theMainRoad = new TheMainRoad(18, 10, 19, 50);
    private static News news3 = new News(20, 0, 21, 30);
    private static SpecialReport specialReport2 = new SpecialReport(21, 40, 22, 50);
    private static BeachVolleyball beachVolleyball = new BeachVolleyball(23, 0, 1, 30);
    private static Football football = new Football(1, 40, 3, 50);
    private static News news4 = new News(4, 0, 6, 0);

    private static Broadcast[] broadcasts = {news, allForTheMatch, news2, specialReport, fighterWithoutRules,
            thereIsATheme, theMainRoad, news3, specialReport2, beachVolleyball, football, news4};

    public MatchTV(int numChanel) {
        super(numChanel, "МатчТВ", broadcasts);
    }
}
