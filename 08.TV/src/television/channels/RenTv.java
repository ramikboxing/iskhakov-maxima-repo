package television.channels;

import television.Broadcast;
import television.ChannelTV;
import television.broadcast.renTV.*;


public class RenTv extends ChannelTV {

    private static ResidentEvil residentEvil = new ResidentEvil(0, 5, 2, 10);
    private static TheMostShockingHypotheses theMostShockingHypotheses = new TheMostShockingHypotheses(2, 20, 3, 0);
    private static ChapmanSecrets chapmanSecrets = new ChapmanSecrets(3, 20, 4, 35);
    private static DocumentaryProject documentaryProject = new DocumentaryProject(4, 45, 5, 40);
    private static HowTheWorldWorks howTheWorldWorks = new HowTheWorldWorks(5, 50, 6, 50);
    private static GoodMorning goodMorning = new GoodMorning(7, 0, 7, 50);
    private static News news = new News(8, 0, 9, 30);
    private static EmergencyCall emergencyCall = new EmergencyCall(9, 50, 11, 30);
    private static MilitarySecret militarySecret = new MilitarySecret(11, 40, 13, 0);
    private static SecurityCouncil securityCouncil = new SecurityCouncil(13, 30, 14, 50);
    private static ResidentEvil residentEvil2 = new ResidentEvil(15, 0, 18, 0);
    private static News news1 = new News(18, 0, 19, 30);
    private static MilitarySecret militarySecret2 = new MilitarySecret(19, 40, 21, 0);
    private static StarshipTroopers starshipTroopers = new StarshipTroopers(21, 10, 23, 10);
    private static News news2 = new News(23,15,0,0);

    private static Broadcast[] broadcasts = {residentEvil, theMostShockingHypotheses, chapmanSecrets, documentaryProject,
            howTheWorldWorks, goodMorning, news, emergencyCall, militarySecret, securityCouncil, residentEvil2, news1,
            militarySecret2, starshipTroopers, news2};

    public RenTv(int numChanel) {
        super(numChanel, "РенТВ", broadcasts);
    }
}
