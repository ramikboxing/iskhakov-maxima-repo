package television.channels;

import television.Broadcast;
import television.ChannelTV;
import television.broadcast.CTC.*;

public class CTC extends ChannelTV {

    private static Eralash eralash = new Eralash(6, 0, 8, 0);
    private static ThreeCats threeCats = new ThreeCats(8, 10, 9, 50);
    private static TomAndJerry tomAndJerry = new TomAndJerry(10, 0, 11, 50);
    private static EnchantedElla enchantedElla = new EnchantedElla(12, 0, 14, 10);
    private static RapuncelComplicatedStory rapuncelComplicatedStory = new RapuncelComplicatedStory(14, 30, 16, 20);
    private static Sisters sisters = new Sisters(16, 30, 18, 0);
    private static Voroniny voroniny = new Voroniny(18, 15, 20, 0);
    private static SixFrames sixFrames = new SixFrames(20, 15, 21, 30);
    private static RodKom rodKom = new RodKom(21, 40, 22, 50);
    private static JupiterAscending jupiterAscending = new JupiterAscending(23, 0, 1, 50);

    private static Broadcast[] broadcasts = {eralash, threeCats, tomAndJerry, enchantedElla, rapuncelComplicatedStory,
            sisters, voroniny, sixFrames, rodKom, jupiterAscending};

    public CTC(int numChanel) {
        super(numChanel, "CTC", broadcasts);
    }
}
