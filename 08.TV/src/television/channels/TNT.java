package television.channels;

import television.Broadcast;
import television.ChannelTV;
import television.broadcast.TNT.*;

public class TNT extends ChannelTV {
    private static OpenMicrophone openMicrophone = new OpenMicrophone(0, 0, 2, 0);
    private static OnceInRussia onceInRussia = new OnceInRussia(2, 10, 3, 30);
    private static StarsInAfrica starsInAfrica = new StarsInAfrica(3, 40, 5, 0);
    private static GoldOfGelendzhik goldOfGelendzhik = new GoldOfGelendzhik(5, 15, 7, 0);
    private static BuzovaInTheKitchen buzovaInTheKitchen = new BuzovaInTheKitchen(7, 10, 9, 0);
    private static University university = new University(9, 10, 10, 30);
    private static Olga olga = new Olga(10, 40, 11, 50);
    private static SashaTanya sashaTanya = new SashaTanya(12, 0, 13, 30);
    private static Family family = new Family(13, 40, 15, 0);
    private static MissionMiami missionMiami = new MissionMiami(15, 10, 17, 30);
    private static ComedyClub comedyClub = new ComedyClub(17, 40, 19, 0);
    private static ComedyWoman comedyWoman = new ComedyWoman(19, 10, 20, 50);
    private static Improvisation improvisation = new Improvisation(21, 0, 22, 30);
    private static OnceInRussia onceInRussia2 = new OnceInRussia(22, 40, 23, 55);

    private static Broadcast[] broadcasts = {openMicrophone, onceInRussia, starsInAfrica, goldOfGelendzhik,
            buzovaInTheKitchen, university, olga, sashaTanya, family, missionMiami, comedyClub, comedyWoman,
            improvisation, onceInRussia2};

    public TNT(int numChanel) {
        super(numChanel, "ТНТ", broadcasts);
    }
}
