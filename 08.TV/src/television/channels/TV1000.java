package television.channels;

import television.Broadcast;
import television.ChannelTV;
import television.broadcast.TV1000.*;

public class TV1000 extends ChannelTV {
    private static DrHouse drHouse = new DrHouse(5, 0, 7, 0);
    private static EdwardScissorhands edwardScissorhands = new EdwardScissorhands(7, 20, 9, 0);
    private static BrothersGrimm brothersGrimm = new BrothersGrimm(9, 20, 10, 50);
    private static OnceUponATimeInRome onceUponATimeInRome = new OnceUponATimeInRome(11, 0, 13, 10);
    private static Gifted gifted = new Gifted(13, 25, 15, 40);
    private static TheSecretOfTheSevenSisters theSecretOfTheSevenSisters = new TheSecretOfTheSevenSisters(15, 50, 17, 50);
    private static LordOfTheRings lordOfTheRings = new LordOfTheRings(18, 0, 23, 15);

    private static Broadcast[] broadcasts = {drHouse, edwardScissorhands, brothersGrimm, onceUponATimeInRome,
            gifted, theSecretOfTheSevenSisters, lordOfTheRings};

    public TV1000(int numChanel) {
        super(numChanel, "ТВ-1000", broadcasts);
    }
}
