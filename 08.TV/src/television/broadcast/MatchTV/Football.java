package television.broadcast.MatchTV;

import television.Broadcast;

public class Football extends Broadcast {
    public Football(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                    int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Футбол", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
