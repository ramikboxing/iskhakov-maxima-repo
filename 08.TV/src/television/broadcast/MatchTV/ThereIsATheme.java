package television.broadcast.MatchTV;

import television.Broadcast;

public class ThereIsATheme extends Broadcast {
    public ThereIsATheme(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                         int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Есть тема", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
