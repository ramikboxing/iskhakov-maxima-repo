package television.broadcast.MatchTV;

import television.Broadcast;

public class TheMainRoad extends Broadcast {
    public TheMainRoad(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                       int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Главная дорога", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
