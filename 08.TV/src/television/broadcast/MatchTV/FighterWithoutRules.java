package television.broadcast.MatchTV;

import television.Broadcast;

public class FighterWithoutRules extends Broadcast {
    public FighterWithoutRules(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                               int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Боец без правил. х/ф", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
