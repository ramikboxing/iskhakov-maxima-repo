package television.broadcast.MatchTV;

import television.Broadcast;

public class AllForTheMatch extends Broadcast {
    public AllForTheMatch(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                          int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Все на матч. Прямой эфир", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
