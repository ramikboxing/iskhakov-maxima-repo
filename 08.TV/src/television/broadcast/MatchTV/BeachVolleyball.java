package television.broadcast.MatchTV;

import television.Broadcast;

public class BeachVolleyball extends Broadcast {
    public BeachVolleyball(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                           int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Пляжный волебол", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
