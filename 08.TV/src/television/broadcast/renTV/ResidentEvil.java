package television.broadcast.renTV;

import television.Broadcast;

public class ResidentEvil extends Broadcast {
    public ResidentEvil(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                        int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Обитель зла", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
