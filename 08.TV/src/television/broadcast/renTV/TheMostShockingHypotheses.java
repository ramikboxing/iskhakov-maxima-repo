package television.broadcast.renTV;

import television.Broadcast;

public class TheMostShockingHypotheses extends Broadcast {
    public TheMostShockingHypotheses(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                                     int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Самые шокирующие гипотезы", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
