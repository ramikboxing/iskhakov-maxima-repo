package television.broadcast.renTV;

import television.Broadcast;

public class News extends Broadcast {
    public News(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Новости", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
