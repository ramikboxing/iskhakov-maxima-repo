package television.broadcast.renTV;

import television.Broadcast;

public class SecurityCouncil extends Broadcast {
    public SecurityCouncil(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                           int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("СовБез", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
