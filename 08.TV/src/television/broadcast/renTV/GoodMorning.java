package television.broadcast.renTV;

import television.Broadcast;

public class GoodMorning extends Broadcast {
    public GoodMorning(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                       int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("С бодрым утром", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
