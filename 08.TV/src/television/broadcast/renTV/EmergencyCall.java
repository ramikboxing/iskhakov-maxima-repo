package television.broadcast.renTV;

import television.Broadcast;

public class EmergencyCall extends Broadcast {
    public EmergencyCall(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                         int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Экстренный вызов", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
