package television.broadcast.renTV;

import television.Broadcast;
import television.ChannelTV;

public class MilitarySecret extends Broadcast {

    public MilitarySecret( int broadcastTimeHourStart, int broadcastTimeMinuteStart, int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Военая тайна с Игорем Прокапенко", broadcastTimeHourStart, broadcastTimeMinuteStart, broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
