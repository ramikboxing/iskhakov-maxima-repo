package television.broadcast.renTV;

import television.Broadcast;

public class HowTheWorldWorks extends Broadcast {
    public HowTheWorldWorks( int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                             int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Как устроен Мир", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
