package television.broadcast.renTV;

import television.Broadcast;

public class DocumentaryProject extends Broadcast {
    public DocumentaryProject(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                              int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Документальный проект", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
