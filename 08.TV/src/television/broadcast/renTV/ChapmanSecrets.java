package television.broadcast.renTV;

import television.Broadcast;

public class ChapmanSecrets extends Broadcast {
    public ChapmanSecrets( int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                           int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Тайны Чапман", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
