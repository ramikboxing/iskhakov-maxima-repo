package television.broadcast.renTV;

import television.Broadcast;

public class StarshipTroopers extends Broadcast {
    public StarshipTroopers( int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                             int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Звездный десант", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
