package television.broadcast.CTC;

import television.Broadcast;

public class SixFrames extends Broadcast {
    public SixFrames(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                     int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Шесть кадров", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
