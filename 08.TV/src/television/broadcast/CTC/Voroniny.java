package television.broadcast.CTC;

import television.Broadcast;

public class Voroniny extends Broadcast {
    public Voroniny(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                    int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Воронины", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
