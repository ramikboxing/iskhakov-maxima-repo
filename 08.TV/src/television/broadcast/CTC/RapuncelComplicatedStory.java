package television.broadcast.CTC;

import television.Broadcast;

public class RapuncelComplicatedStory extends Broadcast {
    public RapuncelComplicatedStory(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                                    int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Рапунцель. Запутанная история", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
