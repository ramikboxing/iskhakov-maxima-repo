package television.broadcast.CTC;

import television.Broadcast;

public class JupiterAscending extends Broadcast {
    public JupiterAscending(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                            int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Восхождение Юпитер", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
