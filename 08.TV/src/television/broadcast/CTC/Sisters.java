package television.broadcast.CTC;

import television.Broadcast;

public class Sisters extends Broadcast {
    public Sisters(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                   int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Сестры", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
