package television.broadcast.CTC;

import television.Broadcast;

public class TomAndJerry extends Broadcast {
    public TomAndJerry(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                       int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Том и Джери", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
