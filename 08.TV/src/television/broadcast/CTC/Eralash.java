package television.broadcast.CTC;

import television.Broadcast;

public class Eralash extends Broadcast {
    public Eralash(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                   int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Ералаш", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
