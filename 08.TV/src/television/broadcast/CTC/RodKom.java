package television.broadcast.CTC;

import television.Broadcast;

public class RodKom extends Broadcast {
    public RodKom(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                  int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("РодКом", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
