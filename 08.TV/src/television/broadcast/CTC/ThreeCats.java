package television.broadcast.CTC;

import television.Broadcast;

public class ThreeCats extends Broadcast {
    public ThreeCats(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                     int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Три кота", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
