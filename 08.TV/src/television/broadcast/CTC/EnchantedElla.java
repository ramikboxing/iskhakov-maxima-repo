package television.broadcast.CTC;

import television.Broadcast;

public class EnchantedElla extends Broadcast {
    public EnchantedElla(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                         int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Закалдованная Элла", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
