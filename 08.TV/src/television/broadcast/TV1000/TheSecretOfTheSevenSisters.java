package television.broadcast.TV1000;

import television.Broadcast;

public class TheSecretOfTheSevenSisters extends Broadcast {
    public TheSecretOfTheSevenSisters(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                                      int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Тайна семи сестер ", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
