package television.broadcast.TV1000;

import television.Broadcast;

public class Gifted extends Broadcast {
    public Gifted(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                  int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Одаренная", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
