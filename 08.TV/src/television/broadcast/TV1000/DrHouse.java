package television.broadcast.TV1000;

import television.Broadcast;

public class DrHouse extends Broadcast {
    public DrHouse(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                   int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Доктор Хаус", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
