package television.broadcast.TV1000;

import television.Broadcast;

public class BrothersGrimm extends Broadcast {
    public BrothersGrimm(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                         int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Братья Гримм", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
