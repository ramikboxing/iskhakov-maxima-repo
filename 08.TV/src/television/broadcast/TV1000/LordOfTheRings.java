package television.broadcast.TV1000;

import television.Broadcast;

public class LordOfTheRings extends Broadcast {
    public LordOfTheRings(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                          int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Властелин колец. Трилогия", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
