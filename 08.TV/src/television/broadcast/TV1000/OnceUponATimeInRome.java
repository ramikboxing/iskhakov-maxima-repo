package television.broadcast.TV1000;

import television.Broadcast;

public class OnceUponATimeInRome extends Broadcast {
    public OnceUponATimeInRome(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                               int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Однажды в Риме", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
