package television.broadcast.TV1000;

import television.Broadcast;

public class EdwardScissorhands extends Broadcast {
    public EdwardScissorhands(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                              int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Эдвард руки-ножницы", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
