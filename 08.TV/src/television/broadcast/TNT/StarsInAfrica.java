package television.broadcast.TNT;

import television.Broadcast;

public class StarsInAfrica extends Broadcast {
    public StarsInAfrica(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                         int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Звезды в Африке", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
