package television.broadcast.TNT;

import television.Broadcast;

public class GoldOfGelendzhik extends Broadcast {
    public GoldOfGelendzhik(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                            int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Золото Геленджика", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
