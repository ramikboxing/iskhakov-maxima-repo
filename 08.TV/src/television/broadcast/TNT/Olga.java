package television.broadcast.TNT;

import television.Broadcast;

public class Olga extends Broadcast {
    public Olga(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Олга. Сезон 4", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
