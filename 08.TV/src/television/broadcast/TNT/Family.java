package television.broadcast.TNT;

import television.Broadcast;

public class Family extends Broadcast {
    public Family(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                  int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Семья", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
