package television.broadcast.TNT;

import television.Broadcast;

public class ComedyClub extends Broadcast {
    public ComedyClub(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                      int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Камеди Клаб", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
