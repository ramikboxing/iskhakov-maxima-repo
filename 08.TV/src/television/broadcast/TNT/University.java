package television.broadcast.TNT;

import television.Broadcast;

public class University extends Broadcast {
    public University(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                      int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Универ. Новая общага", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
