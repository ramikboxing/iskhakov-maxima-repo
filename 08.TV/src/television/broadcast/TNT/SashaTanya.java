package television.broadcast.TNT;

import television.Broadcast;

public class SashaTanya extends Broadcast {
    public SashaTanya(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                      int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Саша Таня", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
