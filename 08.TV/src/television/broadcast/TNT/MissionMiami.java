package television.broadcast.TNT;

import television.Broadcast;

public class MissionMiami extends Broadcast {
    public MissionMiami(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                        int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Миссия Майями", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
