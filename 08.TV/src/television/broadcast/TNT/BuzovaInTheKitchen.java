package television.broadcast.TNT;

import television.Broadcast;

public class BuzovaInTheKitchen extends Broadcast {
    public BuzovaInTheKitchen(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                              int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Бузова на кухне", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
