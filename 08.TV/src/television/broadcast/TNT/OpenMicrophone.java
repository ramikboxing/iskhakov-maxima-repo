package television.broadcast.TNT;

import television.Broadcast;

public class OpenMicrophone extends Broadcast {
    public OpenMicrophone( int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                           int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Открытый микрофон", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
