package television.broadcast.TNT;

import television.Broadcast;

public class OnceInRussia extends Broadcast {
    public OnceInRussia( int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                         int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Однажды в России", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
