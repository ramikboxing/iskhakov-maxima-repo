package television.broadcast.TNT;

import television.Broadcast;

public class ComedyWoman extends Broadcast {
    public ComedyWoman(int broadcastTimeHourStart, int broadcastTimeMinuteStart,
                       int broadcastTimeHourEnd, int broadcastTimeMinuteEnd) {
        super("Камеди Вуман", broadcastTimeHourStart, broadcastTimeMinuteStart,
                broadcastTimeHourEnd, broadcastTimeMinuteEnd);
    }
}
