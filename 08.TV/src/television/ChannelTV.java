package television;

public abstract class ChannelTV {

    protected int numChanel;
    protected String nameChanel;
    protected Broadcast[] broadcasts;

    public int getNumChanel() {
        return numChanel;
    }

    public ChannelTV(int numChanel, String nameChanel, Broadcast[] broadcasts) {
        this.numChanel = numChanel;
        this.nameChanel = nameChanel;
        this.broadcasts = broadcasts;
    }

}
