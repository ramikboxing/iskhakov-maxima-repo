package television;

import remote.IphoneController;
import remote.RadioRemoteImpl;
import remote.TVRemote;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Нажмите цифру");
        // Теже цифры но отрицательные - это управление с iPhone
        System.out.println("с 1 по 5 : выбор канала на \n" +
                "6: следующий канал\n" +
                "7: предыдущий канал\n" +
                "8: последний канал\n" +
                "9: выключить телевизор");
        TV tvSamsung = new TV("Samsung");
        TVRemote remote = new RadioRemoteImpl(tvSamsung);
        TVRemote iRemote = new IphoneController(tvSamsung);
        tvSamsung.on();
        while (tvSamsung.isTurnTV()) {
            int x = scanner.nextInt();
            if (x > 0) {
                if (x == 9) {
                    remote.off();
                    continue;
                } else if (x == 1) {
                    remote.switchToChannel(1);
                    continue;
                } else if (x == 2) {
                    remote.switchToChannel(2);
                    continue;
                } else if (x == 3) {
                    remote.switchToChannel(3);
                    continue;
                } else if (x == 4) {
                    remote.switchToChannel(4);
                    continue;
                } else if (x == 5) {
                    remote.switchToChannel(5);
                    continue;
                } else if (x == 6) {
                    remote.nextChannel();
                    continue;
                } else if (x == 7) {
                    remote.previousChannel();
                    continue;
                } else if (x == 8) {
                    remote.lastChannel();
                    continue;
                }
            } else {
                if (x == -1) {
                    iRemote.switchToChannel(1);
                    continue;
                } else if (x == -2) {
                    iRemote.switchToChannel(2);
                    continue;
                } else if (x == -3) {
                    iRemote.switchToChannel(3);
                    continue;
                } else if (x == -4) {
                    iRemote.switchToChannel(4);
                    continue;
                } else if (x == -5) {
                    iRemote.switchToChannel(5);
                    continue;
                } else if (x == -6) {
                    iRemote.nextChannel();
                    continue;
                } else if (x == -7) {
                    iRemote.previousChannel();
                    continue;
                } else if (x == -8) {
                    iRemote.lastChannel();
                    continue;
                } else if (x == -9) {
                    iRemote.off();
                }
            }
        }//while
    }
}
