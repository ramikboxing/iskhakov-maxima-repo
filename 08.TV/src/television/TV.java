package television;

import television.channels.*;

import java.util.Date;

public class TV {

    private String modelTV;
    private ChannelTV lastChannel;
    private boolean turnTV;
    private ChannelTV channel;

    RenTv renTv = new RenTv(1);
    TNT tnt = new TNT(2);
    CTC ctc = new CTC(3);
    MatchTV matchTV = new MatchTV(4);
    TV1000 tv1000 = new TV1000(5);
    ChannelTV[] channelArr = {renTv, tnt, ctc, matchTV, tv1000};

    public TV(String modelTV) {
        this.modelTV = modelTV;
    }

    public boolean isTurnTV() {
        return turnTV;
    }

    public void on() {
        int x = ((int) Math.random() * 4);
        lastChannel = searchChannel(x);
        showChannel(lastChannel);
        turnTV = true;
    }

    public void off() {
        turnTV = false;
    }

    public ChannelTV searchChannel(int numChannel) {
        for (ChannelTV channel : channelArr) {
            if (numChannel == channel.numChanel) {
                return channel;
            }
        }
        return channelArr[0];
    }

    public void showChannel(ChannelTV channel) {
        Date realTime = new Date();
        this.channel = channel;
        boolean showAdvertising = true;
        for (int i = 0; i < channel.broadcasts.length; i++) {
            if (((realTime.getHours() == channel.broadcasts[i].broadcastTimeHourStart) &&
                    (realTime.getMinutes() >= channel.broadcasts[i].broadcastTimeMinuteStart)) ||
                    ((realTime.getHours() > channel.broadcasts[i].broadcastTimeHourStart))) {
                if (((realTime.getHours() == channel.broadcasts[i].broadcastTimeHourEnd) &&
                        (realTime.getMinutes() <= channel.broadcasts[i].broadcastTimeMinuteEnd)) ||
                        (realTime.getHours() < channel.broadcasts[i].broadcastTimeHourEnd)) {
                    System.out.println(channel.nameChanel + "\n" + channel.broadcasts[i].broadcastName);
                    showAdvertising = false;
                } else {
                    continue;
                }
            } else {
                continue;
            }
        }//for
        if (showAdvertising) {
            Advertising advertising = new Advertising(0, 0, 0, 0);
            System.out.println(channel.nameChanel + "\n" + advertising.broadcastName);
        }
    }

    public void nextChannel() {
        for (int i = 0; i < channelArr.length; i++) {
            if (channel.getNumChanel() == channelArr[channelArr.length - 1].getNumChanel()) {
                lastChannel = channel;
                channel = channelArr[0];
                showChannel(channel);
                break;
            }
            if (channel.getNumChanel() == channelArr[i].getNumChanel()) {
                lastChannel = channel;
                channel = channelArr[++i];
                showChannel(channel);
                break;
            }
        }
    }

    public void previousChannel() {
        lastChannel = channel;
        for (int i = channelArr.length - 1; i >= 0; i--) {
            if (channel.getNumChanel() == channelArr[0].getNumChanel()) {
                lastChannel = channel;
                channel = channelArr[channelArr.length - 1];
                showChannel(channel);
                break;
            }
            if (channel.getNumChanel() == channelArr[i].getNumChanel()) {
                lastChannel = channel;
                channel = channelArr[--i];
                showChannel(channel);
                break;
            }
        }
    }

    public void lastChannel() {
        ChannelTV last = channel;
        showChannel(lastChannel);
        lastChannel = last;
    }

}
