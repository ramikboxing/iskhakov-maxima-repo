import java. io.*;
import java.util.Scanner;
import java.util.InputMismatchException;

class ArrayMethods{

	/*3.1 Четные и нечетные числа
	На вход программы поступает целочисленный массив. Нужно сначала распечатать только четные числа массива, потом - нечетные.*/
	private void evenAndOddNumbers(int [] array) {

		System.out.println("\033[H\033[J");
		System.out.print("Массив: ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i]+" ");
		}
		System.out.print("\nЧетные числа массива: ");
		for(int i = 0; i < array.length; i++){
			if(array[i] % 2 == 0){
				System.out.print(array[i]+" ");
			}
		}
		System.out.print("\nНечетные числа массива: ");
		for(int i = 0; i < array.length; i++){
			if(array[i] % 2 != 0){
				System.out.print(array[i]+" ");
			}
		}
			System.out.println();
	}

	/*3.2 Поиск числа
	На вход программы поступает целочисленный массив. Нужно написать отдельный метод, который принимает в скобки массив, и число.
	Метод проверяет есть ли данное число в массиве или нет. Если есть - выводит "true", если нет - "false"*/
	private boolean searchNumFoArray(int [] array, int x ){

		for(int i = 0; i < array.length; i++){
			if(array[i] == x){
				System.out.println(true +" (число "+ x + " присутствует в массиве )");
				return true;
			}
		}
		System.out.println( false + "(число "+ x + " отсутствует в массиве )");
		return false;
	}

	/*3.3 Замена числа
	На вход программы поступает целочисленный массив. Нужно написать отдельный метод, который принимает в скобки массив, и число.
	Метод проверяет, есть ли в массиве данное число, если есть - вместо этого числа ставится 0 (если чисел несколько, то все они обнуляются), 
	если числа нет, то выводится "Данное число отсутствует в массиве"*/
	private void zeroReplacement(int [] array, int x){

		boolean option = true;
		for(int i = 0; i < array.length; i++){
			if(array[i] == x){
				array[i] = 0;
				option = false;
			}
		}//for
		if(option){
			System.out.println("Данное число отсутствует в массиве\n\n");
			return;
		}
		for (int i = 0; i < array.length; i++ ) {
			System.out.print(array[i]+ " ");
		}
		System.out.println("\n");
	}

	/*3.4 Создание и заполнение массива
	В методе .bubbleSortReturned из класса Main5, закомментировать строку с Arrays.copyOf(). 
	После этого, инициализоровать новый массив той же длины и заполнить его значениеми из массива, переданного в метод*/
	public int[] bubbleSortReturned(int[] array) {

        //TODO: Сделать это через цикл
        //Создаем новый массив со значениями старого
        //int[] resultArray = Arrays.copyOf(array, array.length);
		int[] resultArray = new int [array.length];	
        for (int i = 0;i < array.length; i++ ) {
       		resultArray[i] = array[i];
        }
        return resultArray;
    }

	/*3.5.1 Инверсия массива
	На вход программы подается массив. Программа должна вывести значение массива, до "инверсии" и после.*/
	private void inversion (int [] array){

		for (int i = 0; i < array.length; i++ ) {
			System.out.print(array[i]+ " ");
		}

		System.out.print("\n");

		for (int i = array.length - 1; i >= 0; i-- ) {
			System.out.print(array[i]+ " ");
		}

		System.out.println("\n");
	}

	//3.5.2Инверсия массива в рекурсии
	private void recInversion (int [] array, int indx){

		if (indx < 0){
			return;
		}
		System.out.print(array[indx]+" ");
		indx--;
		recInversion(array, indx);
	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner (System.in);
		PrintWriter pw = new PrintWriter(System.out, true);		
		ArrayMethods arrayMethods = new ArrayMethods();
		int [] arrayNums;
		while(true){
			pw.println("Введите числа через запятую");
			String strNums = scanner.nextLine();
			String [] arrayStrNums = strNums.split(",");
			arrayNums = new int [arrayStrNums.length];

			try{
				for (int i = 0; i < arrayNums.length; i++ ) {
					arrayNums[i] = Integer.parseInt(arrayStrNums[i]);
				}
				break;
			}catch (NumberFormatException e) {  
				pw.print("\033[H\033[J");
	        	pw.println("Неправильный формат строки! \nПопробуйте снова"); 
			}
		}//while
		int option = 0;		
			while(true){
				try{
					// создадим новый сканер. т.к. при вводе неверных символов сканер сломается.
					// при следующей итерации будет создан новый сканер.
					Scanner scan = new Scanner (System.in);
					pw.println(); 
					pw.println("Выберети пункт меню");
					pw.println("1 . Вывести четные и нечетные элементы массива.");
					pw.println("2 . Проверить есть ли число в массиве");
					pw.println("3 . Заменить число на 0");
					pw.println("4 . Копия массива в цикле");
					pw.println("5 . Инверсия массива");
					pw.println("9 . Выход");
					
					option = scan.nextInt();						
											
					if (option == 1){
						arrayMethods.evenAndOddNumbers(arrayNums);
						continue;

					}else if (option == 2){
						System.out.println("\033[H\033[J");
						pw.println("Какое число искать");
						int num = scan.nextInt();
						arrayMethods.searchNumFoArray(arrayNums, num);
						continue;						
					}else if (option == 3){	
						System.out.println("\033[H\033[J");
						pw.println("Какое число заменим ?");				
						int num = scan.nextInt();
						arrayMethods.zeroReplacement(arrayNums, num);
						continue;
					}else if (option == 4){	
						System.out.println("\033[H\033[J");			
						int [] newArrayNum = arrayMethods.bubbleSortReturned(arrayNums);
						for (int i = 0; i < newArrayNum.length; i++) {
							pw.print(newArrayNum[i]+", ");							
						}
						pw.println();
						continue;
					}else if (option == 5){
						//arrayMethods.inversion (arrayNums);
						arrayMethods.recInversion(arrayNums, arrayNums.length - 1);
						continue;
					}else if (option == 9){
						break;
					}else {
						pw.print("\033[H\033[J");
						pw.println("\n\nОшибка ввода меню\n\n");
					}
					
				}catch(InputMismatchException e){
					pw.println("\n\nОшибка ввода символа\n\n");
					continue;									
				}						
			}//while
	}//main
}//class


 
    
