# Spring Boot

Добавляем родительский pom.xml файл для Spring Boot

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.7.2</version>
</parent>
```
И зависимость для работы с WEB
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

Чтобы приложение компилировалось в JAR с манифестом нужно добавить плагин:
```xml
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.7.2</version>
            </plugin>
        </plugins>
    </build>
```

### Дополнительный материал:

Страница для старта Spring Boot приложения
https://start.spring.io/

Документация по Spring Boot
https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#web

По-умолчанию, начиная с Spring Boot версии 2.2 расширение файлов .ftlh, а не .ftl 
https://stackoverflow.com/questions/42330870/spring-boot-unable-to-resolve-freemarker-view

Можно создать свой баннер
https://devops.datenkollektiv.de/banner.txt/index.html

Примеры CSS стилей
https://www.sanwebe.com/2014/08/css-html-forms-designs

Документация по операторам freemarker
https://freemarker.apache.org/docs/ref_directive_list.html

Статьи по тестированию
https://spring.io/guides/gs/testing-web/
https://www.baeldung.com/spring-boot-testing
https://mkyong.com/spring-boot/spring-boot-junit-5-mockito/
https://www.javaguides.net/2022/03/spring-boot-unit-testing-service-layer.html

Статья про JaCoCo - библиотека для оценки покрытия кода тестами
https://www.baeldung.com/jacoco