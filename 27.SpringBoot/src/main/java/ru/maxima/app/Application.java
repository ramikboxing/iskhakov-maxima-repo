package ru.maxima.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication // главная аннотация Spring Boot, включающая @SpringBootConfiguration, @EnableAutoConfiguration, @ComponentScan
@ComponentScan(basePackages = "ru.maxima")                      // указываем область сканирования бинов
@EnableJpaRepositories(basePackages = "ru.maxima.repository")   // указываем область сканирования репозиториев
@EntityScan(basePackages = "ru.maxima.entity")                  // указываем область сканирования сущностей
public class Application {                                      // главный класс приложения
    public static void main(String[] args) {                    // главный метод приложения
        SpringApplication.run(Application.class);               // место где запускается приложение
    }
}
