package ru.maxima.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data                                                   // аккуратно использовать, поскольку внутри @toString
@NoArgsConstructor                                      // аннотация для конструктора без параметров
@AllArgsConstructor                                     // аннотация для конструктора со всеми параметрами
@Builder                                                // аннотация для билдера
@Entity                                                 // аннотация для сущности
@Table(name = "user_demo")                                  // указываем название таблицы для сущности
public class User {
    @Id                                                 // специальная аннотация для Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // указываем как генерируем Id
    public Long id;
    @Column(name = "first_name")
    public String name;
    @Column(name = "last_name")
    public String surname;

    public String email;
    public String password;
}

