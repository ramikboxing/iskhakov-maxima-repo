package ru.maxima.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.maxima.entity.User;


public interface UserRepository extends JpaRepository<User, Long> {
}

