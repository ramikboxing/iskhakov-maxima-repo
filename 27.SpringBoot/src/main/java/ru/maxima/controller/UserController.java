package ru.maxima.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.maxima.entity.User;
import ru.maxima.service.UserService;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService service;                                        // внедрение бина сервиса

    @GetMapping("/users")                                            // объявление GET метода с эндпойнтом /users
    public String getUsers(Model model) {                               // передаём модель в качестве параметра метода
        List<User> users = service.findAll();                           // вызываем метод сервиса и сохраняем в users
        model.addAttribute("usersFromController", users);   // передача users через модель
        return "users";                                                 // метод возвращает view templates/users.ftlh
    }
}

