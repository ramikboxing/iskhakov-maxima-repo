package ru.maxima.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.entity.User;
import ru.maxima.repository.UserRepository;

import java.util.List;

@Service                                // создаем бин
public class UserService {

    @Autowired
    private UserRepository repository;  // внедряем бин репозитория

    public List<User> findAll() {
        return repository.findAll();    // вызываем стандартный JPA метод репозитория
    }
}

