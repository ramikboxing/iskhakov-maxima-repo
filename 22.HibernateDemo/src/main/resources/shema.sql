create database "hibernateDB";

CREATE TABLE person (id SERIAL PRIMARY KEY,
                    first_name VARCHAR (30),
                    last_name VARCHAR (30));

ALTER TABLE person ADD COLUMN age INT;

create table cars_person( id SERIAL PRIMARY KEY,
                          model VARCHAR(30),
                          owner_id INTEGER REFERENCES person(id));

CREATE TABLE community_of_motorists(id SERIAL PRIMARY KEY,
                                    name_community VARCHAR(40),
                                    person_id INTEGER REFERENCES person(id));

alter table community_of_motorists drop column person_id;

INSERT INTO community_of_motorists(name_community) VALUES ('KIA_CLUB');
INSERT INTO community_of_motorists(name_community) VALUES ('SUBARU_KZN');
INSERT INTO community_of_motorists(name_community) VALUES ('AUDI_116');

ALTER TABLE person ADD COLUMN club_id INT;

Select * FROM person;
alter table community_of_motorists drop column name_community;

alter table person drop column firstname;
alter table person drop column lastname;
alter table person drop column club_id_id;