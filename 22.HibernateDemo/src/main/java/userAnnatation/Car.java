package userAnnatation;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "cars_person")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Автогенерация поля id
    private Integer id;
    @NonNull
    private String model;

    @NonNull
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;



}
