package userAnnatation;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "community_of_motorists")
public class CommunityOfMotorists {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Автогенерация поля id
    private int id;
    @NonNull
    private String nameCommunity;

    @OneToMany
    @JoinColumn(name = "club_id")
    private List<User> usersCommunityList;

}
