package userAnnatation;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "person")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Автогенерация поля id
    private Integer id;
    @NonNull
    private String first_name;
    @NonNull
    private String last_name;
    @NonNull
    private Integer age;

    @OneToMany
    private List<Car> cars;

    @ManyToOne
    @JoinColumn(name = "club_id")
    private CommunityOfMotorists club;


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", age=" + age +
                '}';
    }
}
