package userXML;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;

// класс User настраивается через xml конфигурацию
public class Main {
    public static void main(String[] args) {

        Configuration configuration = new Configuration();
        configuration.setProperty("hibernate.connection.url", "jdbc:postgresql://localhost:5432/hibernateDB");
        configuration.setProperty("hibernate.connection.username", "postgres");
        configuration.setProperty("hibernate.connection.password", "481632");
        configuration.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");

        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL95Dialect");
        //  configuration.setProperty("hibernate.show_sql", "true");
        configuration.setProperty("hibernate.hbm2ddl.auto", "update");

        configuration.addResource("User.hbm.xml");
        configuration.addAnnotatedClass(Car.class);
        configuration.addAnnotatedClass(CommunityOfMotorists.class);
        // Запуск сессии
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        Session session = sessionFactory.openSession();

//Чтение из БД
        // в рамках сессии мы конструируем HQL запрос к таблице hibernate_user
        // language=HQL
        User user = session.createQuery("from User user where user.id = 1", User.class).getSingleResult();
        System.out.println(user);
//Запись в БД
//        session.beginTransaction();     // отклрыли транзакцию
//        session.save(new User("Donald", "Trump", 70));  // выполняем запись в БД
//        session.getTransaction().commit();  // закрыли транзакцию

        // Выводим все авто из таблицы hibernate_car c помощью HQL
        // language=HQL
        List<Car> carList = session.createQuery("from Car car", Car.class).getResultList();
        System.out.println("Список машин");
        for (Car car : carList) {
            System.out.println(car.toString());
        }
//        session.beginTransaction();
//        session.save("BMW_CLUB_KZN");
//        session.getTransaction().commit();

        // language=HQL
        List<CommunityOfMotorists> communityOfMotoristsList =
                session.createQuery("from CommunityOfMotorists community", CommunityOfMotorists.class).getResultList();
        System.out.println("Список сообществ автолюбителей");
        for (CommunityOfMotorists comm : communityOfMotoristsList) {
            System.out.println(comm.toString());
            System.out.println("количество членов "+ comm.getUsersCommunityList().size());
            List<User> users = comm.getUsersCommunityList();
            for (User person : users) {
                System.out.println(person);
            }
        }
    }
}
