# Spring

### Hibernate

[Документация по конфигурации](http://hibernate-refdoc.3141.ru/ch3.Configuration)

**Обязательные параметры:**
hibernate.connection.driver_class   JDBC driver class
hibernate.connection.url            JDBC URL
hibernate.connection.username       имя пользователя JDBC
hibernate.connection.password       пароль пользователя JDBC

**Необязательные параметры:**
hibernate.dialect       Позволяет Hibernate генерировать SQL, оптимизированный для конкретной реляционной базы данных
hibernate.show_sql      Пишет все инструкции SQL в консоль
hibernate.hbm2ddl.auto  Автоматически проверяет или экспортирует DDL схемы в базу данных когда SessionFactory создан

ORM (object-relational mapping) - инструмент совмещения объектов в java коде и таблиц в базах данных

Структура работы Hibernate

SQL & HQL

XML & Annotation mapping

# Домашнее задание 
Создать третью сущность, записать и прочитать. (Маппинг через аннотации)
Третья сущность должна содержать список List<User> users.