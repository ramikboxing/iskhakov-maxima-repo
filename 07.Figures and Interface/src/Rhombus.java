//Параллелограм ABCD является ромбом тогда и только тогда:
// 1. Две его смежные стороны равны (AB = BC = CD = AD)
// 2. Его диагонали пересекаются под прямым углом
// 3. Одна из диагоналей делит содержащие ее углы попалам.
// 4. Все стороны равны
// 5. Противоположные углы равны
public class Rhombus extends Parallelogram {

    public Rhombus(int x, int y, double a, int injectionA) {
        super(x, y, a, a, injectionA);
    }
}
