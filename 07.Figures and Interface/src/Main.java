import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) {
        Trapezoid trapezoid = new Trapezoid(0, 5, 6, 6, 7, 8, 30, 50);
        trapezoid.getPerimeter();
        trapezoid.getSquare();
        System.out.println("");

        Parallelogram parallelogram = new Parallelogram(10, 6, 5,7, 78);
        parallelogram.getPerimeter();
        parallelogram.getSquare();
        System.out.println("");

        Rectangle rectangle = new Rectangle(2,3, 5,6);
        rectangle.getPerimeter();
        rectangle.getSquare();
        System.out.println("");

        Square square = new Square(2,2,5);
        square.getPerimeter();
        square.getSquare();
        System.out.println("");
        square.getCoordinates();
        square.move(57,78);
        square.getCoordinates();
        System.out.println("");

        Rhombus rhombus = new Rhombus(6,-6, 14, 65);
        rhombus.getPerimeter();
        rhombus.getSquare();
        System.out.println("");

        Oval oval = new Oval(0,0,34,68);
        oval.getPerimeter();
        oval.getSquare();
        oval.getCoordinates();
        oval.move(57,46);
        oval.getCoordinates();
        System.out.println("");

        Ellipse ellipse = new Ellipse(1,1,34, 68);
        ellipse.getPerimeter();
        ellipse.getSquare();
        System.out.println("");

        Circle circle = new Circle(5, 5, 12);
        circle.getPerimeter();
        circle.getSquare();
        System.out.println("");

        Triangle triangle = new Triangle(3,2,4.0,8.0,10.0);
        triangle.getPerimeter();
        triangle.getSquare();
    }
}
