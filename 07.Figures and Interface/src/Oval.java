public class Oval extends Figure implements Moveable {

    protected double radius1;
    protected double radius2;

    public Oval(int x, int y, double radius1, double radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    @Override
    public void getCoordinates() {
        System.out.println(className + " x - " + x + " y - " + y);
    }

    @Override
    public double getPerimeter() {
        double p = 2 * Math.PI * Math.sqrt(((radius1 * radius1) + (radius2 * radius2)) / 2);
        System.out.println(className + " Периметр = " + p);
        return p;
    }


    @Override
    public double getSquare() {
        double s = 3.14 * radius1 * radius2;
        System.out.println(className + " Площадь = " + s);
        return s;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
