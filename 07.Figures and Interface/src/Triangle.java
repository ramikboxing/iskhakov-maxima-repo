public class Triangle extends Figure implements Moveable {

    private double a;
    private double b;
    private double c;
    private double injectionA;
    private double injectionB;
    private double injectionC;

    //Треугольник по двум сторонам и углу между ними
    public Triangle(int x, int y, double a, double b, int injectionC) {
        super(x, y);
        this.a = a;
        this.b = b;
        double radians = Math.toRadians(injectionC);
        this.c = Math.sqrt((a * a + b * b) - (2 * b * a) * Math.cos(radians));
        this.injectionC = injectionC;
        double cosA = (((b * b) + (c * c) - (a * a)) / (2 * b * c));
        this.injectionA = Math.toDegrees(Math.acos(cosA));
        this.injectionB = 180 - injectionA - injectionC;
    }

    //Треугольник по трем сторонам
    public Triangle(int x, int y, double a, double b, double c) {
        super(x, y);
        this.a = a;
        this.b = b;
        this.c = c;
        double cosA = ((b * b) + (c * c) - (a * a)) / (2 * b * c);
        double cosB = ((a * a) + (c * c) - (b * b)) / (2 * a * c);
        this.injectionA = Math.toDegrees(Math.acos(cosA));
        this.injectionB = Math.toDegrees(Math.acos(cosB));
        this.injectionC = 180 - injectionA - injectionB;
    }

    @Override
    public void getCoordinates() {
        System.out.println(className + " x - " + x + " y - " + y);
    }

    @Override
    public double getPerimeter() {
        double x = a + b + c;
        System.out.println(className + " Периметр = " + x);
        return x;
    }

    @Override
    public double getSquare() {
        double radian = Math.toRadians(injectionC);
        double s = 0.5 * a * b * Math.sin(radian);
        System.out.println(className + " Площадь " + s);
        return 0;
    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
