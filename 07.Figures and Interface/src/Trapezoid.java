// Трапеция выпуклый четырухугольник у которого две стороны паралельны
// Сумма внутрених углов прилежащих к боковой сторое равна 180 градусам
public class Trapezoid extends Figure {

    protected double a;
    protected double b;
    private double c;
    private double d;
    protected int injectionA;
    private int injectionB;
    private int injectionC;
    private int injectionD;

    public Trapezoid(int x, int y, double a, double b, double c, double d, int injectionA, int injectionC) {
        super(x, y);
        if (a > 0 && b > 0 && c > 0 && d > 0 && injectionA > 0 && injectionC > 0
                && injectionA < 180 && injectionC < 180) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.injectionA = injectionA;
            this.injectionB = 180 - injectionA;
            this.injectionC = injectionC;
            this.injectionD = 180 - injectionC;
        } else {
            System.out.println("Ошибка парамтров объекта  " + className);
            this.a = 0;
            this.b = 0;
            this.c = 0;
            this.d = 0;
            this.injectionA = 0;
            this.injectionB = 0;
            this.injectionC = 0;
            this.injectionD = 0;
        }
    }

    @Override
    public void getCoordinates() {
        System.out.println(className + " x - " + x + " y - " + y);
    }

    @Override
    public double getPerimeter() {
        double p = a + b + c + d;
        System.out.println(className + " Периметр = " + p);
        return p;
    }

    @Override
    public double getSquare() {
        double s = ((a + b) / 2) * Math.sqrt((c * c) - (((a - b) * (a - b)) + (c * c) - (d * d)) / 2 * (a - b));
        System.out.println(className + " Площадь = " + s);
        return s;
    }

}
