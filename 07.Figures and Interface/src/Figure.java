public abstract class Figure {
    protected String className = this.getClass().getName();
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public abstract void getCoordinates();

    public abstract double getPerimeter();

    public abstract double getSquare();
}
