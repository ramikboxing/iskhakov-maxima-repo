// Прямоугольник : паралельные стороны равны
// Все углы 90 градусов
public class Rectangle extends Parallelogram {
    public Rectangle(int x, int y, double a, double b) {
        super(x, y, a, b,90);
    }
}
