public interface Moveable {
     abstract void move(int x, int y);
}
