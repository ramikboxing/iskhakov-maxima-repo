//Крадрат можно расматреть как частный случай ромба или прямоугольника
//Все стороны равны (Ромб)
//Все углы 90 градусов
public class Square extends Rectangle implements Moveable {
    // Наследование от прямоугольника
    public Square(int x, int y, double a) {
        super(x, y, a, a);
    }
    /*
    Наследование от ромба
     public Square (int x, int y, double a){
         super(x ,y ,a, 90);
    }
    */
    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
