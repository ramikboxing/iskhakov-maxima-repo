// Паралелограмм - четырехугольник у которого противоположные стороны попарно паралельны
// 1. Противоположные углы равны
// 2. Углы прилежащие к одной стороне в сумме дают 180 градусов
public class Parallelogram extends Trapezoid {
    public Parallelogram(int x, int y, double a, double b, int injectionA) {
        super(x, y, a, b, a, b, injectionA, injectionA);
    }
    @Override
    public double getSquare() {
        double radian = Math.toRadians(injectionA);
        double s = a * b * Math.sin(radian);
        System.out.println(className + " Площадь = " + s);
        return s;
    }
}
