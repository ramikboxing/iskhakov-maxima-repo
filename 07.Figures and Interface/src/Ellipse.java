public class Ellipse extends Oval {

    public Ellipse(int x, int y, double radius1, double radius2) {
        super(x, y, radius1, radius2);
    }
}
