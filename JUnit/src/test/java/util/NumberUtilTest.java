package util;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
public class NumberUtilTest {
    private final NumberUtil numberUtil = new NumberUtil();

    @Nested // для вложенного тестеровочного класса (после анотации появились стрелки запуска)
    @DisplayName("Тестируем isSimple() ")
    class IsSimpleTest {
        @Test
        public void testIsSimple() {
            int number = 31;
            boolean actual = numberUtil.isSimple(number);
            assertEquals(true, actual);
        }

        @ParameterizedTest
        @ValueSource(ints = {2, 3, 13, 17, 19, 31})
        public void testOnSimples(int number) {
            assertTrue(numberUtil.isSimple(number));
        }

        @ParameterizedTest
        @ValueSource(ints = {15, 18, 21, 32})
        public void testOnNotSimples(int number) {
            assertFalse(numberUtil.isSimple(number));
        }

        @ParameterizedTest
        @ValueSource(ints = {0, 1})
        public void testException(int number) {
            assertThrows(IncorrectException.class, () -> numberUtil.isSimple(number));
        }
    }

    @Nested // для вложенного тестеровочного класса (после анотации появились стрелки запуска)
    @DisplayName("Тестируем gcd() ")
    public class GcdTest {
        private final NumberUtil numberUtil = new NumberUtil();

        @ParameterizedTest(name = "throws exception on {0} , {1}")
        @CsvSource(value = {"-18, 12", "9, -12", "64, -48"})
        public void negative_numbers_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numberUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return true on {0} , {1}")
        @CsvSource(value = {"0, 16", "0, 22", "0, 256"})
        public void if_a_null_return_b(int a, int b) {
            assertTrue(b == numberUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return true on {0} , {1}")
        @CsvSource(value = {"16, 16", "22, 22", "256, 256"})
        public void if_a_equals_b_return_b(int a, int b) {
            assertTrue(b == numberUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return true on {0} , {1}")
        @CsvSource(value = {"23, 27", "29, 34", "38, 37", "43, 46", "49,53", "83, 86", "57, 58", " 59, 67",
                "68,69", " 73,76", "74, 97", "78,89", "83, 94", "89,87"})
        public void ugly_numbers(int a, int b) {
            assertTrue(numberUtil.gcd(a, b) == 1);
        }

        @ParameterizedTest(name = "Проверяем ожидание и результат {0} , {1}, {2}")
        @CsvSource(value = {"18,12,6", "9,12,3", "64,48,16"})
        public void testGcd(int a, int b, int expected) {
            assertEquals(expected, numberUtil.gcd(a, b));
        }
    }

}

