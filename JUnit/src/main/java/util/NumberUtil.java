package util;

public class NumberUtil {
    public boolean isSimple(int number) {
        if (number <= 1) {
            throw new IncorrectException();
        }
        if (number == 2 || number == 3) {
            return true;
        }
        for (int i = 4; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int gcd(int a, int b) {
        //18,12=6//9,12=3//64,48=16
        if (a < 0 || b < 0) {
            throw new IllegalArgumentException();
        }
        if (a == 0 || a == b) {
            return b;
        }
        while (b != 0) {
            if (a > b) {
                a -= b;
            } else {
                b -= a;
            }
        }
        return a;
    } //            return a == 0 ? b : gcd(b % a, a);
}
