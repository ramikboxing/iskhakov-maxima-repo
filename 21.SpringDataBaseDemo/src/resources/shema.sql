create table user_demo (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR (30),
    last_name VARCHAR (30)
);
create table cars(
    id SERIAL PRIMARY KEY,
    model VARCHAR(30),
    owner_id INTEGER REFERENCES user_demo(id)
);
INSERT INTO user_demo(first_name, last_name) VALUES ('Ilon', 'Mask');