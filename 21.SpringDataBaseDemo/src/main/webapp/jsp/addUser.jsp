<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="nav-bar">
    <div class="topnav" id="myTopnav">
        <a href="/car">add Car</a>
        <a href="/updateCar">update Car</a>
        <a href="/deleteCarsTable">delete Cars</a>
    </div>
</nav>

<div class="form-style-2">
    <div class="form-style-2-heading">
        Please add user
    </div>
    <form method="post" action="/user">
        <label for="first-name">First Name
            <input class="input-field" type="text" id="first-name" name="first-name">
        </label>
        <label for="last-name">Last Name
            <input class="input-field" type="text" id="last-name" name="last-name">
        </label>
        <input type="submit" value="Add user">
    </form>
</div>

</body>
</html>
