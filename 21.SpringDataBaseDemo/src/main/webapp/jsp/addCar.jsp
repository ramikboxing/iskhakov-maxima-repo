<%--
  Created by IntelliJ IDEA.
  User: ramik
  Date: 06.08.2022
  Time: 9:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>add Car</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="nav-bar">
        <div class="topnav" id="myTopnav">
    <a href="/user">add User</a>
    <a href="/updateCar">update Car</a>
     <a href="/deleteCarsTable">delete Cars</a>
    </div>
</nav>

<div class="form-style-2">
    <div class="form-style-2-heading">
        Please add car
    </div>
    <form method="post" action="/car">
        <label for="model">Model
            <input class="input-field" type="text" id="model" name="model">
        </label>
        <label for="owner">Owner Id
            <input class="input-field" type="text" id="owner" name="owner_id">
        </label>
        <input type="submit" value="Add car">
    </form>
</div>
</body>
</html>
