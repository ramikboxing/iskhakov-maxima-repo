<%--
  Created by IntelliJ IDEA.
  User: ramik
  Date: 08.08.2022
  Time: 21:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>update Car</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="nav-bar">
    <div class="topnav" id="myTopnav">
        <a href="/car">add Car</a>
        <a href="/user">add User</a>
        <a href="/deleteCarsTable">delete Cars</a>
    </div>
</nav>

<div class="form-style-2">
    <div class="form-style-2-heading">
        Please update car the owner
    </div>
    <form method="post" action="/updateCar">
        <label for="new_model">Model
            <input class="input-field" type="text" id="new_model" name="newModel">
        </label>
        <label for="owner">Owner Id
            <input class="input-field" type="text" id="owner" name="owner_id">
        </label>
        <input type="submit" value="update">
    </form>
</div>
</body>
</html>
