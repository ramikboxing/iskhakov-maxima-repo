<%--
  Created by IntelliJ IDEA.
  User: ramik
  Date: 08.08.2022
  Time: 21:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>delete Cars table</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="nav-bar">
    <div class="topnav" id="myTopnav">
        <a href="/car">add Car</a>
        <a href="/user">add User</a>
        <a href="/updateCar">update Car</a>
    </div>
</nav>

<div class="form-style-2">
    <div class="form-style-2-heading">
        Please delete cars
    </div>
    <form method="post" action="/deleteCarsTable">
        <input type="submit" value="Delete">
    </form>
</div>
</body>
</html>
