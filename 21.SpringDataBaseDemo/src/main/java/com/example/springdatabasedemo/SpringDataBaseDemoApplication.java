package com.example.springdatabasedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDataBaseDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataBaseDemoApplication.class, args);
    }

}
