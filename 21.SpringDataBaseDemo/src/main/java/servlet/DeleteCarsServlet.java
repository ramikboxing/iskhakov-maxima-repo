package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet("/deleteCarsTable")
public class DeleteCarsServlet extends HttpServlet {
    protected Connection connection = UserServlet.connection;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            //forward - передает запрос req на resp
            req.getServletContext().getRequestDispatcher("/jsp/deleteCarsTable.jsp").forward(req, resp);
        } catch (ServletException e) {
            System.out.println(e.getMessage() + " " + e.getStackTrace());
        } catch (IOException e) {
            System.out.println(e.getMessage() + " " + e.getStackTrace());
        } catch (Exception e) {
            System.out.println(e.getMessage() + " " + e.getStackTrace());
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)  {
        try (Statement statement = connection.createStatement()) {
            String sqlStatement =
                    "DROP TABLE cars2;";
            System.out.println(sqlStatement);
            statement.execute(sqlStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
