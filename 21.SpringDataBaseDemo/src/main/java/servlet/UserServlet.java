package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

@WebServlet("/user")
public class UserServlet extends HttpServlet {

    static protected Connection connection;

    @Override
    public void init() {
        Properties properties = new Properties(); // для записи настроек из db.properties
        // после компиляции путь к файлу db.properties в папке target немного изменится будет в classes
        String dbFile = "/WEB-INF/classes/db.properties";

        try (FileInputStream reader = new FileInputStream(getServletContext().getRealPath(dbFile))) {
            properties.load(reader);
            String dbUrl = properties.getProperty("db.ulr");
            String dbUsername = properties.getProperty("db.username");
            String dbPassword = properties.getProperty("db.password");
            String dbDriver = properties.getProperty("db.driverClassName");
            Class.forName(dbDriver);
            connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        try {
            //forward - передает запрос req на resp
            req.getServletContext().getRequestDispatcher("/jsp/addUser.jsp").forward(req, resp);
        } catch (ServletException e) {
            System.out.println(e.getMessage() + " " + e.getStackTrace());
        } catch (IOException e) {
            System.out.println(e.getMessage() + " " + e.getStackTrace());
        } catch (Exception e) {
            System.out.println(e.getMessage() + " " + e.getStackTrace());
        }
/*      Аналогичный код
        ServletContext context = request.getServletContext();
        RequestDispatcher dispatcher = context.getRequestDispatcher("/jsp/addUser.jsp");
        dispatcher.forward(request, response);
*/
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

        String firstName = req.getParameter("first-name");
        String lastName = req.getParameter("last-name");

        try (Statement statement = connection.createStatement()) {
            String sqlStatement =
                    "INSERT INTO user_demo(first_name, last_name) VALUES ('" + firstName + "', '" + lastName + "');";
            System.out.println(sqlStatement);
            statement.execute(sqlStatement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
