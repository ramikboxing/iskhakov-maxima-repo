package model;

public class Car {
    private int id;
    private String model;
    private int ownerId;

    public Car(int id, String model, int ownerId) {
        this.id = id;
        this.model = model;
        this.ownerId = ownerId;
    }
}
