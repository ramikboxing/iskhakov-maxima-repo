package autoWash;

public class Car extends Thread {
    AutoWash autoWash = AutoWash.getInstance();
    private String numCar;
    private boolean isClear = false;

    public Car(String numCar) {
        this.numCar = numCar;
        start();
    }

    public String getNumCar() {
        return numCar;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (autoWash) {
                while (autoWash.getSize() <= 0) {
                    try {
                        System.out.println(this.getNumCar() + " ожидает в очереди свободное место");
                        autoWash.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } //while (autoWash.getSize() == 0)
                if (this.isClear) {
                    autoWash.notify();
                    break;
                }
            }
            autoWash.setSize(autoWash.getSize() - 1);
            try {
                System.out.println(this.getNumCar() + " Заезжает в бокс");
                sleep(3000);
                System.out.println(this.getNumCar() + " Моется");
                sleep(3000);
                System.out.println(this.getNumCar() + " Протираем кузов");
                sleep(3000);
                System.out.println(this.getNumCar() + " Выезжает");
                sleep(3000);
                isClear = true;
                autoWash.setSize(autoWash.getSize() + 1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

