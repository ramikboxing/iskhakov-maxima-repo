package autoWash;

public class AutoWash {
    private static AutoWash instance;
    private static int size = 3;

    private AutoWash() {

    }

    public static AutoWash getInstance() {
        if (instance == null) {
            instance = new AutoWash();
        }
        return instance;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        AutoWash.size = size;
    }

}

