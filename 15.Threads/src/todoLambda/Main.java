package todoLambda;

import java.util.concurrent.Exchanger;

public class Main {
    public static void main(String[] args) {
//2. В коде exchanger/runnable/Main.java передедлать реализацию под лямбда-выражения.
        Exchanger<String> exchanger = new Exchanger<>();
        //TODO
        //Lambda
        Runnable anketa = () -> {
            try {
                System.out.println(exchanger.exchange(null));
                Thread.sleep(5000);
                System.out.println(exchanger.exchange(null));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        Runnable person = () -> {
            try {
                exchanger.exchange("Рамиль");
                exchanger.exchange("Исхаков");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };
        new Thread(anketa).start();
        new Thread(person).start();



    }
}
