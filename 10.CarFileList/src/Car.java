
public class Car {

    private int id;
    private String carBrand;
    private String carColor;
    private int enginePower;

    private int mileage;
    private boolean inStock;
    private boolean isDamaged;

    Car(int id, String carBrand, String carColor, int enginePower,
        int mileage, boolean inStock, boolean isDamaged) {
        this.id = id;
        this.carBrand = carBrand;
        this.carColor = carColor;
        this.enginePower = enginePower;

        this.mileage = mileage;
        this.inStock = inStock;
        this.isDamaged = isDamaged;
    }

    public int getId() {
        return id;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public String getCarColor() {
        return carColor;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public int getMileage() {
        return mileage;
    }

    public boolean inStock() {
        return inStock;
    }

    public boolean isDamaged() {
        return isDamaged;
    }
}
