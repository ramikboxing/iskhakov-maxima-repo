import java.util.List;

public interface CarsRepository {
    List<Car> findAll();
    void save (Car car);
    Car searchCarById(int id);
}
