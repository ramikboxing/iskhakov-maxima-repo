import java.io.*;
import java.util.*;

public class CarsRepositoryFileImpl implements CarsRepository {
    private String file;

    public CarsRepositoryFileImpl(String file) {
        this.file = file;
    }

    private List<Car> sortList() {
        List<Car> carsList = findAll();
        carsList.sort(Comparator.comparing(Car::getId));
        return carsList;
    }

    @Override
    public List<Car> findAll() {
        BufferedReader reader = null;
        List<Car> carsList = new ArrayList<>();
        try {
            // Читалка FileReader передает данные в буферную переменную
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String brand = parts[1];
                String color = parts[2];
                int enginePower = Integer.parseInt(parts[3]);
                int mileage = Integer.parseInt(parts[4]);

                boolean inStock;
                if (parts[5].equals("В наличие")) {
                    inStock = true;
                } else {
                    inStock = false;
                }
                boolean isDamaged;
                if (parts[6].equals("Битый")) {
                    isDamaged = true;
                } else {
                    isDamaged = false;
                }
                Car newCar = new Car(id, brand, color, enginePower, mileage, inStock, isDamaged);
                carsList.add(newCar);
                line = reader.readLine();
            }
        } catch (IOException e) {
        } finally {
            try {
                reader.close();
            } catch (IOException ignore) {
            }
        }
        return carsList;
    }

    //2. Предусмотреть функцию из первого уровня и поиск машины по ID
    @Override
    public Car searchCarById(int id) {
        List<Car> cars = findAll();
        int lowIndex = 0;
        int highIndex = cars.size() - 1;
        int position = -1;
        while (lowIndex <= highIndex) {
            int midIndex = (lowIndex + highIndex) / 2;
            if (id == cars.get(midIndex).getId()) {
                position = midIndex;
                return cars.get(position);
            } else if (id < cars.get(midIndex).getId()) {
                highIndex = midIndex - 1;
            } else if (id > cars.get(midIndex).getId()) {
                lowIndex = midIndex + 1;
            }
        }
        return null;
    }

    @Override
    public void save(Car car) {
        BufferedWriter writer = null;
        String strInStock;
        String strIsDamage;
        try {
            if (car.inStock() == true) {
                strInStock = "В наличие";
            } else {
                strInStock = "Нет в наличие";
            }
        } catch (NullPointerException e) {
            strInStock = "Нет инфомации";
        }
        try {
            if (car.isDamaged() == true) {
                strIsDamage = "Битый";
            } else {
                strIsDamage = "Не битый";
            }
        } catch (NullPointerException e) {
            strIsDamage = "Нет информации";
        }

        try {
            writer = new BufferedWriter(new FileWriter(file, true));
            writer.write(car.getId() + "|" + car.getCarBrand() + "|" + car.getCarColor() + "|" + car.getEnginePower()
                    + "|" + car.getMileage() + "|" + strInStock + "|" + strIsDamage);
            writer.newLine(); // переход на новую строку
            writer.flush(); // записать из буфера в файл
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close(); // закрытие потока
                } catch (IOException ignore) {
                }
            }
        }
    }
}
