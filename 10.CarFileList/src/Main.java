import java.io.IOException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static Car createCar() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("ID автомоиля_ ");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Марка автомобиля_ ");
        String brand = scanner.nextLine();
        System.out.print("Цвет автомобиля_ ");
        String color = scanner.nextLine();
        System.out.print("Мощность двигателя_ ");
        int enginePower = scanner.nextInt();
        System.out.print("Пробег_ ");
        int mileage = scanner.nextInt();
        scanner.nextLine();
        System.out.println(" Авто в наличие ?  (Да(Yes)/Нет(No)_ ");
        String strInStock = scanner.nextLine();
        boolean inStock;
        if ("Да".equals(strInStock) || "Yes".equals(strInStock)) {
            inStock = true;
        } else {
            inStock = false;
        }
        System.out.println("Авто битый? (Да(Yew)/Нет(No)_ ");
        String strIsDamage = scanner.nextLine();
        boolean isDamage;
        if ("Да".equals(strIsDamage) || "Yes".equals(strIsDamage)) {
            isDamage = true;
        } else {
            isDamage = false;
        }
        Car car = new Car(id, brand, color, enginePower, mileage, inStock, isDamage);
        return car;
    }

    public static void main(String[] args) {
        String file = "cars.txt";
        CarsRepository carsRepository = new CarsRepositoryFileImpl(file);

        while (true) {
            try {
                Scanner scanner = new Scanner(System.in);
                System.out.println("1. печатать список автомобилей");
                System.out.println("2. поиск автомобилей");
                System.out.println("3. добавить автомобиль в список");
                System.out.println("9. выход");

                int option = scanner.nextInt();
                if (option == 1) { //печать списка автомобилей
                    List<Car> cars = carsRepository.findAll();
                    for (Car car : cars) {
                        System.out.println(car.getCarBrand());
                    }
                } else if (option == 2) {
                    System.out.print("Введите Id для поиска_");
                    int byId = scanner.nextInt();
                    Car car = carsRepository.searchCarById(byId);
                    System.out.println(car.getCarBrand());
                } else if (option == 3) { //добавление авто
                    carsRepository.save(createCar());
                } else if (option == 9) {
                    break;
                }
            } catch (InputMismatchException e) {
                System.out.println("Ошибка Ввода " + e);
            } catch (Exception e) {
                System.out.println("Неизвесная ошибка" + e);
            }
        }//while
    }
}
