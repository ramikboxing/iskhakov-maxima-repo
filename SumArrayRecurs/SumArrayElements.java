/*Нужно реализовать программу, которая принимает на вход массив целых чисел.
  Программа должна высчитать сумму всех элементов с помощью рекурсии*/
import java. io.*;
import java.util.Scanner;
import java.util.InputMismatchException;

class SumArrayElements{

	public static int recyrsMethod(int[] arr, int indx){
		if(indx == 0){
			return arr[0];
		}else{
			return arr[indx]+recyrsMethod(arr, --indx);
		}		
	}

	public static void main(String[] args) {

		Scanner scanner = new Scanner (System.in);
		PrintWriter pw = new PrintWriter(System.out, true);		
		
		int [] arrayNums;
		while(true){
			pw.println("Введите числа через запятую");
			String strNums = scanner.nextLine();
			String [] arrayStrNums = strNums.split(",");
			arrayNums = new int [arrayStrNums.length];

			try{
				for (int i = 0; i < arrayNums.length; i++ ) {
					arrayNums[i] = Integer.parseInt(arrayStrNums[i]);
				}
				break;
			}catch (NumberFormatException e) {  
				pw.print("\033[H\033[J");
	        	pw.println("Неправильный формат строки! \nПопробуйте снова"); 
			}
		}//while
			System.out.println("Сумма элементов:"+ recyrsMethod(arrayNums, arrayNums.length-1));
	}//main
}//class


 
    
