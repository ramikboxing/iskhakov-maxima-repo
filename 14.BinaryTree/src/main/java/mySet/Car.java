package mySet;

import lombok.*;

import java.util.Objects;

@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@ToString

public class Car {
    @NonNull
    private String carBrand;
    @NonNull
    private String carModel;
    @Setter
    @NonNull
    private String carColor;
    @Setter
    private int enginePower;
    @NonNull
    private String vinNumber;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return vinNumber.equals(car.vinNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(vinNumber);
    }
}
