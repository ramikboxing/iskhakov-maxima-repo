package mySet;

public interface MySet<T> {
    /**
     * Метод добаляет element в первый свободный индекс
     * после добавления размер (size) колекции увеличивается
     *
     * @param element - элемент для добавления в колекцию
     */
    boolean add(T element);

    /**
     * Метод возвращает размер колекции
     *
     * @return количество элементов в колекции
     */
    int size();

    /**
     * Метод возвращает элемент из колекции по index
     *
     * @param index
     * @return elements [index]
     */
    T get(int index);

    /**
     * Удаление элемента из колекции по index
     * После удаления размер (size) уменьшается.
     *
     * @param index
     */
    void remove(int index);

    /**
     * Метод возвращает длинну ячеек колекции
     *
     * @return elements.length
     */
    int length();

    /**
     * Печать элементов из колекции
     */
    void print();

}
