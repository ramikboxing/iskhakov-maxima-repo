package mySet;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;


public class MySetImpl<T> implements MySet<T> {
    private static final int DEFAULT_SIZE = 5;
    private T[] elements = (T[])new Object[DEFAULT_SIZE];
    private int size;

    private void resize() {
        T[] oldArray = this.elements;
        this.elements = (T[]) new Object[size + 5];
        for (int i = 0; i < size; i++) {
            this.elements[i] = oldArray[i];
        }
    }

    private boolean isFull() {
        return size == elements.length;
    }

    @Override
    public boolean add(T element) {
        if (isFull()) {
            resize();
        }
        for (int i = 0; i < elements.length; i++) {
            if (element.equals(elements[i])) {
                return false;
            }
            if (elements[i] == null) {
                for (int j = i + 1; j < elements.length; j++) {
                    if (element.equals(elements[i])) {
                        return false;
                    }
                }
                this.elements[i] = element;
                size++;
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        return elements[index];
    }

    @Override
    public void remove(int index) {
        elements[index] = null;
        size--;
    }

    @Override
    public int length() {
        return elements.length;
    }

    @Override
    public void print() {
        for (int i = 0; i < elements.length; i++) {
            System.out.println(i + " " + elements[i]);
        }
    }
}
