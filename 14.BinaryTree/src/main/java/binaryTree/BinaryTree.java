package binaryTree;

public class BinaryTree<T extends Number> {

    private final Node root;
    String str;
    String stm;
    int size = 1;

    public BinaryTree(T generic) {
        this.root = new Node(generic);
    }

    public Node getRoot() {
        return root;
    }

    public boolean insert(T t) {
        Node data = new Node(t);
        if (this.root.getData().getClass().getName() == data.getData().getClass().getName()) {
            Node node = root;
            push(node, data);
            size++;
            return true;
        }
        return false;
    }

    private void push(Node node, Node data) {
        if (data.getData().doubleValue() <= node.getData().doubleValue()) {
            if (node.left == null) {
                node.left = new Node(data.getData());
                node.left.setLevel(node.getLevel() + 1);
            } else {
                push(node.left, data);
            }
            return;
        } else if (data.getData().doubleValue() > node.getData().doubleValue()) {
            if (node.right == null) {
                node.right = new Node(data.getData());
                node.right.setLevel(node.getLevel() + 1);
                return;
            } else {
                push(node.right, data);
            }
            return;
        }
    }

    private void print(Node node) {
        str = "----.";
        stm = "";
        for (int i = 0; i < node.getLevel(); i++) {
            stm += str;
        }
        System.out.println(stm + node.getData() + " level " + node.getLevel());
    }

    //Алгоритмы в глубь имеют три типа обхода: Pre-order, In-order,
    /**
     * Pre-order стоит использовать тогда, когда вы не знете что вам нужно, провереть узлы перед тем как проверить их
     * листья
     * @param node
     */
    public void preOrder(Node node){
        if (node == null) return;
        print(node);
        preOrder(node.left);
        preOrder(node.right);
    }

    /**
     * In-order обход используется как раз когда нам надо проверять в начале детей и только потом подыматься
     * к родительским узлам
     * Выводит элементы по возрастанию
     * @param node
     */
    public void inOrder(Node node) {
        if (node == null) return;
        inOrder(node.left);
        print(node);
        inOrder(node.right);
    }

    /**
     * Post-order самый забавный случай - это когда нам нужно начать-так сказать с листов и завершить главным корнем,
     * т.е. разлодить дерево на то, как оно строилось
     * @param node
     */
    public void postOrder(Node node){
        if (node == null) return;
        postOrder(node.left);
        postOrder(node.right);
        print(node);
    }
}

