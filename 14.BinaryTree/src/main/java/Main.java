//1.Изучить "Красное-Черное дерево".
//2.Написать свое бинарное дерево. Объект класса, должен принимать в себя элементы (в разнобой). После добавления всех
// элементов, вывести в консоль System.out.println(myBinaryTree) -> Элементы должны вывестись отсортировано
// (не по тому порядку, по которому их клали, а отсортировано).
//3.Реализовать свой Set. Сделать свою автотеку. У каждой машины, есть свой VIN (уникальный идентификатор). Реализовать свой класс, который добавляет в себя только уникальные машины.
//Две разные машины , но с одним VIN он считает, как одну, соответственно, ее добавлять нельзя. Используемая структура данных - массив

import binaryTree.BinaryTree;
import mySet.Car;
import mySet.MySetImpl;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        // Создаем бинарное дерево и помещаем в него корень (Тип данных числовой, лшюбой наследник Number,
        // далее добавляемые данные должны быть того же типа.
        BinaryTree biTree = new BinaryTree(5.0f);
        Random random = new Random();
        for (int i = 0; i < 15; i++) {
            biTree.insert(random.nextFloat() * 10);
        }
        //Вывод элементов по возрастанию
        biTree.inOrder(biTree.getRoot());

        //Set
        Car audi = new Car("Audi", "A6", "White", "V125ND500");
        Car bmw = new Car("BMW", "X6", "Black", "V125ND500");
        Car bmw2 = new Car("BMW", "X6", "Black", "V125ND501");
        Car opel = new Car("Opel", "Astra", "Yellow", "V125ND502");
        Car honda = new Car("Honda", "Acord", "Blue", "V125ND504");
        MySetImpl mysSet = new MySetImpl();
        mysSet.add(audi);
        mysSet.add(bmw);
        mysSet.add(bmw2);
        mysSet.add(opel);
        mysSet.add(honda);
        // Один автомобиль не добавится
        mysSet.print();
    }
}
