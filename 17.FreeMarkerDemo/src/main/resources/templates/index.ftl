<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>HomeWork/17</title>
</head>
<body>
<p>Hello ${name}! You have the following messages:</p>
<#list messages as m>
    <p><b>${m.from}:</b> ${m.body}</p>
</#list>
</body>
</html>