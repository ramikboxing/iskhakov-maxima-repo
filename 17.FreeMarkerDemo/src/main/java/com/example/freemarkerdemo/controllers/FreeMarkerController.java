package com.example.freemarkerdemo.controllers;

import com.example.freemarkerdemo.models.MessageModel;
import com.example.freemarkerdemo.servise.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class FreeMarkerController {
    private final Message messages;

@Autowired
    public FreeMarkerController(Message mes) {
        this.messages = mes;
    }

    @GetMapping("/greeting/{name}")
    public String greeting(@PathVariable("name") String name, Model model) {
        String nameStr = name;
        List<MessageModel> mesList = messages.getListMessage();
        model.addAttribute("name", nameStr);
        model.addAttribute("messages", mesList);
        return "index";
    }
}
