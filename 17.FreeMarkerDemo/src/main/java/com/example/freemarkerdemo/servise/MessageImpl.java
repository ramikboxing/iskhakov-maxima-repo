package com.example.freemarkerdemo.servise;

import com.example.freemarkerdemo.models.MessageModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class MessageImpl implements Message{

    @Override
    public List<MessageModel> getListMessage() {
        List<MessageModel> listMessage = new ArrayList<>();
        listMessage.add( new MessageModel("Tim", "Please don't forget to bring the conference papers!"));
        listMessage.add( new MessageModel("Cindy", "Can you give me visit afternoon?"));
        listMessage.add( new MessageModel("Richard", "Man, this time don't forget the papers!"));
        return listMessage;
    }
}
