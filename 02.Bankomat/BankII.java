/*История - нам нужно снять деньги с банкомата.
Данные на входе программы:
Мы вводим число 7850;
Данные на выходе программы:
Выдано (Для 7850):

1 уровень сложности:
5000 руб. - 1 шт.
2000 руб - 1 шт.
1000 руб - 0 шт.
500 руб. - 1 шт.
200 руб. - 1 шт.
100 руб. - 1 шт.
50 руб. - 1 шт.
10 руб - 0 шт.

2 уровень сложности:
Купюры, которые не участвуют в выдаче - не выводить
5000 руб. - 1 шт.
2000 руб - 1 шт.
500 руб. - 1 шт.
200 руб. - 1 шт.
100 руб. - 1 шт.
50 руб. - 1 шт.

3 уровень сложности:
Добавить возможность размена (Выдать либо крупными, либо мелочью)

4 уровень сложности:
Добавить проверку на наличие необходимой суммы на счету.
Если снимаемая сумма больше суммы на счету, то выводить ("Недостаточно средств на счету")*/
import java.util.Scanner;

class BankII {

    private int balance = 100000;

    public int getBalance(){
		return balance;
	}

	public void setBalance(int balance){
		this.balance = balance;
	}

	private void giveOutCashMax(int money){

		int[]denominations = {5000, 1000, 500, 200, 100, 50, 10, 5, 2, 1};
		System.out.println ("Снято со счета :");
		for (int i = 0; i < denominations.length; i++ ) {
			if (money >= denominations[i]){
				System.out.println(denominations[i] + " руб. - " + money / denominations[i] + "шт.");
	    		money %= denominations[i];
	    	}			
		}	
	}

	private void giveOutCashMin(int money){

		int[]denominations = {5000, 1000, 500, 200, 100, 50, 10, 5, 2, 1};
		System.out.println ("Снято со счета :");
		for (int i =0; i < denominations.length; i++ ){
			if (money > denominations[i]){
				money = cashExchange(money, denominations[i]);
	    	}
		}

	}
	
 	private int cashExchange(int money, int denomination){
		int remainder; //остаток, переходит дальше к выдаче
		int forExchange;  //на размен половину, другую половину дальше к выдаче
		int exchangeRemaider; //еще одна купюра если их кол-во нечетно к сейчас выдаче
			remainder = money % denomination;
			forExchange =  (money / denomination) / 2;
			exchangeRemaider = (money / denomination) % 2;
			System.out.println(denomination + "руб. - " + (forExchange + exchangeRemaider) + "шт.");
			money = remainder + (forExchange * denomination);
			return money;	    
	}

	private boolean newBalance (int money, boolean bool){
		//зачисление средств
		if (bool){
			setBalance(getBalance() + money);
			return true;
		}else{ // снятие средств
			if(money > getBalance()){
				System.out.println("Не достаточно средств на счете !!!");	
				return false;
			}else{
				setBalance(getBalance() - money);
				return true;
			}
		}

	}

	public static void main (String [] args){

		BankII bank = new BankII();
		
		System.out.println(" Здраствуйте на вашем счете "+ bank.getBalance() + " руб.");
		while (true){
			System.out.println ("\n Проверка баланса нажмите 0");	
			System.out.println (" Если желаете пополнить баланс нажмите 1 ");
			System.out.println (" Для снятия наличных 2 ");
			System.out.println (" Завершение работы нажмите -1");
			
			Scanner scanner = new Scanner(System.in);
			int select = scanner.nextInt();
			int money;
			switch (select){
				case 0: // Запрос баланса 
					System.out.print("\033[H\033[J");
					System.out.println ("На вашем счете "+ bank.getBalance());
					break;
				case 1: // Пополнение 
					System.out.print("\033[H\033[J");
					System.out.print("Какую сумму вы положите на счет _");
					money = scanner.nextInt();
					bank.newBalance(money, true);
					break;
				case 2: // Снятие средств 
					System.out.print("\033[H\033[J");
					System.out.println("Введите сумму для снятия");
					money = scanner.nextInt();
					if(!bank.newBalance(money, false)){
						break;
					}
					System.out.println ("Выдать крупными купюрами, нажмите 0");
					System.out.println ("Выдать мелкими купюрами, нажмите 1");
					select = scanner.nextInt();
					switch (select){
						case 0:
							bank.giveOutCashMax(money);
							break;
						case 1:
							bank.giveOutCashMin(money);
							break;
						default:
							System.out.print("\033[H\033[J");
							System.out.println ("\n Введено некоректное значение попробуте все заново \n");
							break;
					}
					break;
				case -1:
					System.out.print("\033[H\033[J");
					System.out.println ("Завершение работы");
					return;	
				default:					
					System.out.print("\033[H\033[J");
					System.out.println ("\n Введено некоректное значение попробуте все заново \n");
					break;
			} // switch
		
		} // while		
		
	}
}