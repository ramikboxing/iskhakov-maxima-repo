/*История - нам нужно снять деньги с банкомата.
Данные на входе программы:
Мы вводим число 7850;
Данные на выходе программы:
Выдано (Для 7850):

1 уровень сложности:
5000 руб. - 1 шт.
2000 руб - 1 шт.
1000 руб - 0 шт.
500 руб. - 1 шт.
200 руб. - 1 шт.
100 руб. - 1 шт.
50 руб. - 1 шт.
10 руб - 0 шт.

2 уровень сложности:
Купюры, которые не участвуют в выдаче - не выводить
5000 руб. - 1 шт.
2000 руб - 1 шт.
500 руб. - 1 шт.
200 руб. - 1 шт.
100 руб. - 1 шт.
50 руб. - 1 шт.

3 уровень сложности:
Добавить возможность размена (Выдать либо крупными, либо мелочью)

4 уровень сложности:
Добавить проверку на наличие необходимой суммы на счету.
Если снимаемая сумма больше суммы на счету, то выводить ("Недостаточно средств на счету")*/
import java.util.Scanner;

class Bank {

	public static void main (String [] args){

  		int balance = 100000;
		
		System.out.println(" Здраствуйте на вашем счете "+ balance + " руб.");

		while (true){
			System.out.println ("\n Проверка баланса нажмите 0");	
			System.out.println (" Если желаете пополнить баланс нажмите 1 ");
			System.out.println (" Для снятия наличных 2 ");
			System.out.println (" Завершение работы нажмите -1");
			
			Scanner scanner = new Scanner(System.in);
			int select = scanner.nextInt();
			int money;
			switch (select){
				case 0: // Запрос баланса 
					System.out.print("\033[H\033[J");
					System.out.println ("На вашем счете "+ balance);
					break;
				case 1: // Пополнение 
					System.out.print("\033[H\033[J");
					System.out.print("Какую сумму вы положите на счет _");
					money = scanner.nextInt();
					balance += money;
					break;
				case 2: // Снятие средств 
					System.out.print("\033[H\033[J");
					System.out.println("Введите сумму для снятия");
					money = scanner.nextInt();
					if(money > balance){
						System.out.println("Не достаточно средств на счете !!!");
						break;
					}
					System.out.println ("Выдать крупными купюрами, нажмите 0");
					System.out.println ("Выдать мелкими купюрами, нажмите 1");
					select = scanner.nextInt();
					switch (select){
						case 0: // Снятие
						    balance -=money;
							System.out.println ("Снято со счета :");	
							if (money >= 5000){
								System.out.println( "5000 руб. - " + money / 5000 + "шт.");
						    	money %= 5000;
						    }
						    if (money >= 1000){
								System.out.println( "1000 руб. - " + money / 1000 + "шт.");
						    	money %= 1000;
						    }
						    if (money >= 500){
								System.out.println( "500 руб. - " + money / 500 + "шт.");
						    	money %= 500;
						    }
						    if (money >= 200){
								System.out.println( "200 руб. - " + money / 200 + "шт.");
						    	money %= 200;
						    }
						    if (money >= 100){
								System.out.println( "100 руб. - " + money / 100 + "шт.");
						    	money %= 100;
						    }
						    if (money >= 50){
								System.out.println( "50 руб. - " + money / 50 + "шт.");
						    	money %= 50;
						    }
						    if (money >= 10){
								System.out.println( "10 руб. - " + money / 10 + "шт.");
						    	money %= 10;
						    }			
							if (money >= 5){
								System.out.println( "5 руб. - " + money / 5 + "шт.");
						    	money %= 5;
						    }
						    if (money >= 2){
								System.out.println( "2 руб. - " + money / 2 + "шт.");
						    	money %= 2;
						    }
						    if (money >= 1){
								System.out.println( "1 руб. - " + money / 1 + "шт.");
							}

							break;
						case 1: // Снятие с разменом
							balance -=money;
							int remainder; //остаток, переходит дальше к выдаче
							int forExchange;  //на размен половину, другую половину дальше к выдаче
							int exchangeRemaider; //еще одна купюра если их кол-во нечетно к сейчас выдаче
							System.out.println ("Снято со счета :");
							if (money > 5000){
								remainder = money % 5000;
								forExchange =  (money / 5000) / 2;
								exchangeRemaider = (money / 5000) % 2;
								System.out.println("5000 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 5000);
					    	}
					    	if (money > 1000){
								remainder = money % 1000;
								forExchange =  (money / 1000) / 2;
								exchangeRemaider = (money / 1000) % 2;
								System.out.println("1000 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 1000);
					    	}
					    	if (money > 500){
								remainder = money % 500;
								forExchange =  (money / 500) / 2;
								exchangeRemaider = (money / 500) % 2;
								System.out.println("500 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 500);
					    	}
					    	if (money > 200){
								remainder = money % 200;
								forExchange =  (money / 200) / 2;
								exchangeRemaider = (money / 200) % 2;
								System.out.println("200 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 200);
					    	}
					    	if (money > 100){
								remainder = money % 100;
								forExchange =  (money / 100) / 2;
								exchangeRemaider = (money / 100) % 2;
								System.out.println("100 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 100);
					    	}
					    	if (money > 50){
								remainder = money % 50;
								forExchange =  (money / 50) / 2;
								exchangeRemaider = (money / 50) % 2;
								System.out.println("50 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 50);
					    	}
					    	if (money > 10){
								remainder = money % 10;
								forExchange =  (money / 10) / 2;
								exchangeRemaider = (money / 10) % 2;
								System.out.println("10 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 10);
							}
					    	if (money > 5){
								remainder = money % 5;
								forExchange =  (money / 5) / 2;
								exchangeRemaider = (money / 5) % 2;
								System.out.println("5 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 5);
					    	}
					    	if (money > 2){
								remainder = money % 2;
								forExchange =  (money / 2) / 2;
								exchangeRemaider = (money / 2) % 2;
								System.out.println("2 руб. - " + (forExchange + exchangeRemaider) + "шт.");
								money = remainder + (forExchange * 2);
					    	}
					    	if (money >= 1){
								System.out.println( "1 руб. - " + money / 1 + "шт.");
								
					    	}
							break;
						default:
							System.out.print("\033[H\033[J");
							System.out.println ("\n Введено некоректное значение попробуте все заново \n");
							break;	
					}//switch
					break;			 
				case -1:
					System.out.print("\033[H\033[J");
					System.out.println ("Завершение работы");
					return;	
				default:					
					System.out.print("\033[H\033[J");
					System.out.println ("\n Введено некоректное значение попробуте все заново \n");
					break;
			} // switch
		
		} // while		
		
	}
}

