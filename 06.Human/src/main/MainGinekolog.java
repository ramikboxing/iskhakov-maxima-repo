package main;

import model.Ginekolog;

public class MainGinekolog {
    public static void main(String[] args) {
        Ginekolog ginekolog = new Ginekolog(25, 56,168,
                "Natali", "Portman", 50);
        System.out.println("Hello my name is "+ ginekolog.getName()+" "+ ginekolog.getLastname());
        ginekolog.says();
        ginekolog.maritalStatus();
        ginekolog.sleep();
        ginekolog.eat();
        ginekolog.maritalStatus();
        ginekolog.says();
        ginekolog.sleep();
        ginekolog.eat();
        ginekolog.job();
        ginekolog.toString();
        System.out.println(ginekolog.toString());
    }

}
