package main;

import model.TaxiMan;

public class MainTaxiMan {
    public static void main(String[] args) {
        TaxiMan taxiMan = new TaxiMan(41, 175, 85,
                "Vitaly", "Ivanov", 56);
        taxiMan.eat();
        taxiMan.sleep();
        taxiMan.job();
    }
}
