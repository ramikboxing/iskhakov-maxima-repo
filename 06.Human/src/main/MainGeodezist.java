package main;

import model.Geodezist;

public class MainGeodezist {
    public static void main(String[] args) {
        Geodezist geodezist = new Geodezist(35, 75, 175, "Valera", "Petrov",
                48, 45);
        System.out.println("Привет я : " + geodezist.getName() + " Фамилия моя " + geodezist.getLastname());
        geodezist.self();
        geodezist.eat();
        geodezist.sleep();
        geodezist.self();
    }
}
