package abstractModel;

import lombok.*;
//lombok анатации заменили все геттеры и сетеры в классе.
//@Getter
//@Setter
@AllArgsConstructor // Замена констрктора который содержит все поля
@Data  // Эта  анатация содержит в себе @ToString, @EqualsAndHashCode, @Getter, @Setter, @RequiredArgsConstructor
public abstract class Human {
    private int age;
    private int weight;
    private int growth;
    private String name;
    private String lastname;

//    public Human(int age, int weight, int growth, String name, String lastname) {
//        this.age = age;
//        this.weight = weight;
//        this.growth = growth;
//        this.name = name;
//        this.lastname = lastname;
//    }

//    public int getAge() {
//        return age;
//    }
//
//    public int getWeight() {
//        return weight;
//    }
//
//    public int getGrowth() {
//        return growth;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getLastname() {
//        return lastname;
//    }
//
//    public void setLastname(String lastname) {
//        this.lastname = lastname;
//    }

    public abstract void sleep();

    public abstract void eat();

    public abstract void job();

    public abstract boolean isAlive();

    public void say() {
        System.out.println("I am Human, i can say.");
    }

}
