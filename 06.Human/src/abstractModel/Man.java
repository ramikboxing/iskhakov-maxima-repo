package abstractModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Man extends Human {
    private int testosteroneLevel;

//    public int getTestosteroneLevel() {
//        return testosteroneLevel;
//    }

//    public void setTestosteroneLevel(int testosteroneLevel) {
//        this.testosteroneLevel = testosteroneLevel;
//    }

    public Man(int age, int weight, int growth, String name, String lastname, int testosteroneLevel) {
        super(age, weight, growth, name, lastname);
        this.testosteroneLevel = testosteroneLevel;
    }

    public abstract void thinks();
}
