package abstractModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Woman extends Human {
    private int estrogenLevel;

//    public int getEstrogenLevel() {
//        return estrogenLevel;
//    }
//
//    public void setEstrogenLevel(int estrogenLevel) {
//        this.estrogenLevel = estrogenLevel;
//    }

    public Woman(int age, int weight, int growth, String name, String lastname, int estrogenLevel) {
        super(age, weight, growth, name, lastname);
        this.estrogenLevel = estrogenLevel;
    }

    public abstract void says();
}
