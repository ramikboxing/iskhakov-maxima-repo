package model;

import abstractModel.Man;
import lombok.AllArgsConstructor;
// Здесь в классе наследнике не могу реализовать конструктор через lombok
public class Geodezist extends Man {

    private String takesMeasurements = "Я еще раскладываю оборудование";

    private int cheerfulness;

    public Geodezist(int age, int weight, int growth, String name, String lastname, int testosteroneLevel,
                     int cheerfulness) {
        super(age, weight, growth, name, lastname, testosteroneLevel);
        this.cheerfulness = cheerfulness;
    }


    @Override
    public void sleep() {
        if (this.isAlive()) {
            this.cheerfulness += 10;
            this.setTestosteroneLevel(getTestosteroneLevel() + 5);
            System.out.println("Геодезист: Поспал, можно и поработать!");
        } else {
            System.out.println("Спит вечым сном");
        }
    }

    @Override
    public void eat() {
        if (this.isAlive()) {
            this.cheerfulness += 5;
            this.setTestosteroneLevel(getTestosteroneLevel() + 3);
            System.out.println("Геодезист: Поел, неплохо было бы вздремнуть");
        } else {
            System.out.println("Персонаж погиб");
        }
    }

    @Override
    public void job() {
        if (this.isAlive()) {
            this.cheerfulness -= 5;
            this.setTestosteroneLevel(getTestosteroneLevel() + 2);
            this.takesMeasurements = "Геодезист: найдены точки границ участка";
        } else {
            System.out.println("Персонаж погиб");
        }
    }

    @Override
    public boolean isAlive() {
        if (cheerfulness > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void thinks() {
        if (this.isAlive()) {
            System.out.println("Сколько геодезиста не корми, а подпаивать придётся.");
            cheerfulness--;
            this.setTestosteroneLevel(getTestosteroneLevel() + 5);
        } else {
            System.out.println("Персонаж погиб");
        }
    }

    public void self() {
        if (getTestosteroneLevel() > 50 && cheerfulness > 50) {
            System.out.println("Я в норме можем поработать");
        } else {
            System.out.println("Чет я запарисля отдохнуть бы");
        }
    }
}
