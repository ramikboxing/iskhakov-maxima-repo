package model;

import abstractModel.Man;
import lombok.Getter;

public class PoliceMan extends Man {
    @Getter
    private int armor;
    @Getter
    private String rank;
    private int accuracy = 30;

    public PoliceMan(int age, int weight, int growth, String name, String lastname, int testosteroneLevel,
                     int armor, String rank) {
        super(age, weight, growth, name, lastname, testosteroneLevel);
        this.armor = armor;
        this.rank = rank;
    }

//    public String getRank() {
//        return rank;
//    }

//    public int getArmor() {
//        return armor;
//    }

    @Override
    public void sleep() {
        if (isAlive()) {
            System.out.println(getRank() + " спит, служба идет");
            this.armor += 5;
            this.accuracy += 15;
            this.setTestosteroneLevel(getTestosteroneLevel() + 2);
        } else {
            System.out.println(getRank() + " " + this.getLastname() + " погиб как герой, и спит вечным сном");
        }
    }

    @Override
    public void eat() {
        if (isAlive()) {
            System.out.println("Пончики самая лучшая еда");
            this.armor += 5;
            this.setTestosteroneLevel(getTestosteroneLevel() + 5);
        } else {
            System.out.println(getRank() + " " + this.getLastname() + " погиб как герой, теперь не до еды");
        }
    }


    @Override
    public void job() {
        for (int i = 0; i < 15; i++) {
            if (isAlive()) {
                System.out.println("Перестрелка");
                if (accuracy < 50) {
                    this.armor -= 5;
                    this.accuracy -= 10;
                    continue;
                } else if (accuracy < 70) {
                    this.armor -= 3;
                    this.accuracy -= 7;
                    continue;
                } else if (accuracy < 100) {
                    this.accuracy -= 5;
                    continue;
                }
            } else {
                System.out.println(getRank() + " " + this.getLastname() +
                        " погиб как герой, для работы найдите другого");
                return;
            }
        }
        System.out.println("Задание выполнено");
    }

    @Override
    public boolean isAlive() {
        if (armor < 0) {
            return false;
        }
        return true;
    }

    @Override
    public void thinks() {
        System.out.println("Сила есть, ума не надо");
        this.armor += 5;
        this.accuracy -= 10;
        this.setTestosteroneLevel(getTestosteroneLevel() - 4);
    }

    public void targetPractice() {
        if ("Private".equals(this.rank)) {
            this.armor += 8;
            this.accuracy += 10;
            this.setTestosteroneLevel(getTestosteroneLevel() + 2);
        } else if ("Sergeant".equals(this.rank)) {
            this.armor += 10;
            this.accuracy += 15;
            this.setTestosteroneLevel(getTestosteroneLevel() + 3);
        } else if ("Major".equals(this.rank)) {
            this.armor += 15;
            this.accuracy += 20;
            this.setTestosteroneLevel(getTestosteroneLevel() + 5);
        }
    }
}
