package model;

import abstractModel.Woman;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString (of = {"age", "name", "lastnane"})
public class Ginekolog extends Woman {
@Getter
@Setter
    private int happinessLevel;


//    public int getHappinessLevel() {
//        return happinessLevel;
//    }
//
//    public void setHappinessLevel(int happinessLevel) {
//        this.happinessLevel = happinessLevel;
//    }

    public Ginekolog(int age, int weight, int growth, String name, String lastname, int estrogenLevel) {
        super(age, weight, growth, name, lastname, estrogenLevel);
        this.happinessLevel = 30;
    }

    @Override
    public void sleep() {
        if (isAlive()) {
            System.out.println(getName() + " " + getLastname() + " спит ZZzzzzzzz");
            setHappinessLevel(getHappinessLevel() + 15);
            this.setEstrogenLevel(getEstrogenLevel() + 15);
        } else {
            System.out.println(getName() + " больше не с нами");
        }
    }

    @Override
    public void eat() {
        if (isAlive()) {
            System.out.println(getName() + " " + getLastname() + " ест");
            setHappinessLevel(getHappinessLevel() + 10);
            this.setEstrogenLevel(getEstrogenLevel() + 10);
        } else {
            System.out.println(getName() + " больше не с нами");
        }
    }

    @Override
    public void job() {
        if (isAlive()) {
            says();
            setHappinessLevel(getHappinessLevel() - 25);
            this.setEstrogenLevel(getEstrogenLevel() - 25);
        } else {
            System.out.println(getName() + " больше не с нами");
        }
    }

    @Override
    public boolean isAlive() {
        if (getHappinessLevel() < 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void says() {
        if (isAlive()) {
            if (getHappinessLevel() > 50) {
                System.out.println(getName() + " " + getLastname() +
                        " Шутит — Скажите, доктор, вы случайно не гинеколог?  — Нет, но посмотреть могу!\n");
            } else {
                System.out.println(" Задолбала эта картина каждый день");
            }
        }
    }

    public void maritalStatus() {
        if (isAlive()) {
            if (getEstrogenLevel() > 50) {
                setLastname("Kingsly");
                System.out.println(getName() + " Выходит замуж и становиться " + getName() + " " + getLastname());
                setHappinessLevel(getHappinessLevel() + 20);
            } else {
                System.out.println(getName() + " не замужем");
                setHappinessLevel(getHappinessLevel() - 5);
            }
        }
    }
}
