package model;

import abstractModel.Man;

public class TaxiMan extends Man {

    private int health;
    private int many;

    public TaxiMan(int age, int weight, int growth, String name, String lastname, int testosteroneLevel) {
        super(age, weight, growth, name, lastname, testosteroneLevel);
        this.many = 200;
        this.health = 100;
    }

    @Override
    public void sleep() {
        if (isAlive()) {
            System.out.println("Пока таксисит спит, деньги проходят мимо");
            this.many -= 100;
            this.health += 20;
        } else {
            System.out.println("персонаж погиб");
        }
    }

    @Override
    public void eat() {
        if (isAlive() && many > 100) {
            System.out.println("Пока таксист ест денег то же не будет");
            this.many -= 100;
            this.health += 30;
        } else {
            System.out.println("Перснаж либо погиб, либо не хватает денег на еду");
        }
    }

    @Override
    public void job() {
        if (isAlive() && health > 50) {
            System.out.println("Таксист вышел позаработать");
            this.many += 200;
            this.health -= 50;
        } else {
            System.out.println("Для работы не достаточно здоровья");
        }
    }

    @Override
    public boolean isAlive() {
        if (health <= 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void thinks() {
        System.out.println("Стоит немного подумать, и все нормально");
        this.many = 50;
        this.health = 50;
    }
}
