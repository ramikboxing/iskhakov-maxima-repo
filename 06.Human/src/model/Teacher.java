package model;

import abstractModel.Woman;

public class Teacher extends Woman {

    private int many;
    private int stress;

    public Teacher(int age, int weight, int growth, String name, String lastname, int estrogenLevel) {
        super(age, weight, growth, name, lastname, estrogenLevel);
        this.many = 200;
        this.stress = 0;
    }

    @Override
    public void sleep() {
        if (isAlive()) {
            stress -= 10;
            if (stress < 0) {
                stress = 0;
            }
        } else {
            System.out.println("Персонаж погиб");
        }
    }

    @Override
    public void eat() {
        if (isAlive()) {
            many -= 100;
        } else {
            System.out.println("Персонаж погиб");
        }
    }

    @Override
    public void job() {
        if (isAlive()) {
            many += 100;
            stress += 50;
        } else {
            System.out.println("Персонаж погиб");
        }
    }

    @Override
    public boolean isAlive() {
        if (stress > 100) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void says() {
        System.out.println("Разговор с подругами");
        many -= 50;
        stress -= 20;
    }
}
