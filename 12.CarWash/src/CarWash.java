import java.util.LinkedList;
import java.util.List;

/**
 * Метод - завезтиМашинуВБокс(Машина машина)
 * Метод - завезтиМашинуВБокс(Машина машина, номер бокса)
 * Метод - помытьМашину() - моет машину (меняет значение переменной чистаяМашина)под индексом 0 (самую первую машину)
 * Метод - помытьМашину(номер бокса) - моет машину под определенным индексом
 * Приватный метод - вывезтиМашинуИзБокса(Машина машина) - убирает машину из списка
 */
public interface CarWash {

   // List<Car> carList = null;
    void putCarInTheBox(Car car);
    void putCarInTheBox(Car car, int numBox);
    void washCar();
    void washCar(int numBox);
}
