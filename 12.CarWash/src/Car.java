public class Car {
    private String carNumber;
    private boolean isClean = false;

    public Car(String carNumber){
        this.carNumber = carNumber;
    }

    public boolean isClean() {
        return isClean;
    }

    public void setClean(boolean clean) {
        isClean = clean;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carNumber='" + carNumber + '\'' +
                ", isClean=" + isClean +
                '}';
    }
}
