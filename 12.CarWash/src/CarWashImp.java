import java.util.Iterator;
import java.util.LinkedList;

public class CarWashImp implements CarWash {
    public static LinkedList<Car> carList = new LinkedList<>();

    /**
     * Метод добавляет в список автомобиль, при условии что список содержит не более 6 авто
     *
     * @param car
     */
    @Override
    public void putCarInTheBox(Car car) {
        if (carList.size() < 6) {
            carList.add(car);
        } else {
            System.out.println("Подождите пока освободится место");
        }
    }

    /**
     * Метод добавляет в список автомобиль, при условии что список содержит не более 6 авто
     * Так как добавление происходит по индексу, индекс не должен превышать размер текущего списка, чтобы избежать
     * исключения
     *
     * @param car
     * @param numBox
     */
    @Override
    public void putCarInTheBox(Car car, int numBox) {
        if (carList.size() < 6) {
            if (carList.size() >= numBox) {
                carList.add(numBox, car);
            } else {
                carList.add(car);
            }
        } else {
            System.out.println("Подождите пока освободится место");
        }
    }

    /**
     * При вызове метода  если очередь не пуста, то первая машина из очереди помечается как "чистая" isClean.
     * и удаляется из очереди
     */
    @Override
    public void washCar() {
        if (carList.peek() != null) {
            Car car = carList.peek();
            car.setClean(true);
            carList.poll();
        }
    }

    @Override
    public void washCar(int numBox) {
        Car car = carList.get(numBox);
        car.setClean(true);
        removeCar(car);
    }

    private void removeCar(Car car) {
        Iterator itrCar = carList.iterator();
        while (itrCar.hasNext()){
              Car c = (Car) itrCar.next();
              if (c.equals(car) && car.isClean()){
                  itrCar.remove();
                }
            }
    }
}


