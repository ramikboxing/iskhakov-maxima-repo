import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Car mazda = new Car("A076KA 116 rus");
        Car honda = new Car("B776BA 116 rus");
        Car toyota = new Car("K777KK 116 rus");
        Car bmw = new Car("O600KK 716 rus");
        Car audi = new Car("A600AA 716 rus");
        Car lexus = new Car("T500TM 116 rus");
        Car renault = new Car("M100TB 116 rus");
        Car volkswagen = new Car("H533EB 716 rus");
        Car dodge = new Car("H568YB 116 rus");
        Car lamborghini = new Car("A070MP 116 rus");

        Car[] allCars = {mazda, honda, toyota, bmw, audi, lexus, renault, volkswagen, dodge, lamborghini};
        System.out.println("Выводим все машины");
        Arrays.stream(allCars)
                .forEach(System.out::println);

        CarWash carWash = new CarWashImp();
        // Загоняем машины в мойку
        carWash.putCarInTheBox(toyota);
        carWash.putCarInTheBox(bmw);
        carWash.putCarInTheBox(audi);
        carWash.putCarInTheBox(mazda, 3);
        carWash.putCarInTheBox(honda, 4);
        carWash.putCarInTheBox(lexus);
        carWash.putCarInTheBox(renault);

        ((CarWashImp) carWash).carList.stream()
                .forEach(System.out::println);
        // Моем машины
        carWash.washCar(); // Моем первую машину в очереди
        carWash.washCar(); // Моем первую машину в очереди
        carWash.washCar(2); // Моем машину во втором боксе;
        System.out.println("машины в мойке");
        ((CarWashImp) carWash).carList.stream()
                        .forEach(System.out::println);
        System.out.println("Все машины");
        Arrays.stream(allCars)
                .forEach(System.out::println);

    }
}
