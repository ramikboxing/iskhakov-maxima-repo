import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// наследуемый HttpServlet не входит по умолчанию, необходимо добавить в pom зависимость из репозитория maven,
// после этого можно импортировать класс HttpServlet
public class MyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try (PrintWriter writer = resp.getWriter()) {
            writer.write("<h1>Servlet Demo Example</h1>");
            System.out.println("Рамиль Сервер запустился");
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
