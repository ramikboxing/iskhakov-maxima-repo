import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// наследуемый HttpServlet не входит по умолчанию, необходимо добавить в pom зависимость из репозитория maven,
// после этого можно импортировать класс HttpServlet
public class CalculatorServlet extends HttpServlet {
    int result = 0;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        result = 0;
        String strX = req.getParameter("param1");
        String strY = req.getParameter("param2");
        if (strX != null || strY != null) {
            int x = Integer.parseInt(req.getParameter("param1"));
            int y = Integer.parseInt(req.getParameter("param2"));
            result = x * y;
        }
        System.out.println(result);

        try (PrintWriter writer = resp.getWriter()) {
            if (result == 0) {
                writer.write("<head> <meta charset=\"UTF-8\"></head>");
                writer.write("<h1>Not params</h1>");
                writer.write("<form method=\"post\" >\n" +
                        "    <label>Param1\n" +
                        "        <input name=\"param1\" placeholder=\"param1\">\n" +
                        "    </label>\n" +
                        "    <label>Param2\n" +
                        "        <input name=\"param2\" placeholder=\"param3\">\n" +
                        "    </label>\n" +
                        "    <label>Result\n" +
                        "        <input name=\"result\" placeholder=" + result + ">\n" +
                        "    </label>\n" +
                        "    <input type=\"submit\" value=\"Go\">\n" +
                        "</form>");

            } else {
                writer.write("<h1>" + "Result = " + result + "</h1>");
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        result = 0;
        String strX = req.getParameter("param1");
        String strY = req.getParameter("param2");
        if (strX != null || strY != null) {
            int x = Integer.parseInt(req.getParameter("param1"));
            int y = Integer.parseInt(req.getParameter("param2"));
            result = x * y;
        }
        System.out.println(result);
        try (PrintWriter writer = resp.getWriter()) {
            if (result == 0) {
                writer.write("<h1>Not params</h1>");
            } else {
                writer.write("<head> <meta charset=\"UTF-8\"></head>");
                writer.write("<form method=\"post\" >\n" +
                        "    <label>Param1\n" +
                        "        <input name=\"param1\" placeholder=" + strX + ">\n" +
                        "    </label>\n" +
                        "    <label>Param2\n" +
                        "        <input name=\"param2\" placeholder= " + strY + " >\n" +
                        "    </label>\n" +
                        "    <label>Result\n" +
                        "        <input name=\"result\" placeholder= " + result + ">\n" +
                        "    </label>\n" +
                        "    <input type=\"submit\" value=\"Go\">\n" +
                        "</form>");
            }
        }

    }
}
