import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;

public class HomeWorkMain {
    /*Домашнее задание:
Вывести значение приватного поля
Изменить значение
setAccessible
set
Вывести измененное значение приватного поля
4**. Создать из aClass объекты с помощью конструкторов через рефлексию*/

    public static void main(String[] args) {
        A objA = new A();
        Class<?> aClass = objA.getClass();
        Field[] publicFields = aClass.getFields();
        Field[] allFields = aClass.getDeclaredFields();
        int lengthPrivateFields = allFields.length - publicFields.length;

        for (int i = 0; i < allFields.length; i++) {
            for (int j = 0; j < publicFields.length; j++) {
                if (allFields[i].equals(publicFields[j])) {
                    allFields[i] = null;
                }
            }
        }
        Field[] privateFields = new Field[lengthPrivateFields];
        for (int i = 0, j = 0; i < allFields.length; i++) {
            if (allFields[i] != null) {
                privateFields[j++] = allFields[i];
                // Метод setAccessible(true) разрешает нам дальнейшую работу c private полями
                privateFields[j - 1].setAccessible(true);
            }
        }

        System.out.println("\nВывод типа, имени и значения приватных полей ДО изменения");
        printFields(privateFields, objA);

        // Изменяем значение
        for (int i = 0; i < privateFields.length; i++) {
            try {
                Field field = privateFields[i];
                String type = field.getType().toString();
                if (type.equals("class java.lang.String")) {
                    privateFields[i].set(objA, "newStrFiled");
                } else if (type.equals("int")) {
                    privateFields[i].set(objA, 200);
                } else if (type.equals("double")) {
                    privateFields[i].set(objA, 200.67);
                } else if (type.equals("boolean")) {
                    privateFields[i].set(objA, true);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();

            }
        }//for

        System.out.println("\nВывод типа, имени и значения приватных полей ПОСЛЕ изменения");
        printFields(privateFields, objA);

        System.out.println("\nСоздать из Class<?> объекты с помощью конструкторов через рефлексию*/");
        System.out.println("ПУСТОЙ Конструктор");
        B newDefaultObj = null;
        try {
            Class<?> clazz = Class.forName("B"); //A.class.getName();
            newDefaultObj = (B) clazz.newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println(newDefaultObj);

        B b = new B();
        Class<?> bClass = b.getClass();
        System.out.println("\nПЕЧАТАЕТМ КОНСТРУКТОРЫ");
        Constructor<?>[] constructors = bClass.getConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.print(constructor.getName() + " (");
            Parameter[] params = constructor.getParameters();
            for (Parameter param : params) {
                System.out.print(param.getType() + " " + param.getName() + "- " + param.isNamePresent() + " ");
            }
            System.out.println(")");
        }
        System.out.println("\nСоздаем объект конструктором с параметрами");
        B objParam = null;
        try {
            objParam = (B) constructors[1].newInstance("newArg0", "newArg1", "newArg2", 10);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        System.out.println("Object B " + objParam);

    }//main

    /**
     * Печать массива полей объекта
     *
     * @param fields - поля объекта
     * @param obj    - объект
     */
    private static void printFields(Field[] fields, Object obj) {
        for (Field field : fields) {
            try {
                System.out.println(field.getType() + " - " + field.getName() + " = " + field.get(obj));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}

