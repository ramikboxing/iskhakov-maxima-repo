import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class LessonMain {
    public static void main(String[] args) {
        A someObject = new A();
        B b = new B();
        //.getClass() - с помощью этого метода можно узнать какого класса этот обьект
        Class<? extends A> aClass = someObject.getClass();
        Class<? extends A> bClass = b.getClass();
        // Получаем имя класса обьекта
        String className = aClass.getName();
        System.out.println("имя класса aClass" + className);
        System.out.println("имя класса bClass" + bClass.getName());

        //.getDeclaredFields(); - возвращает массив типа Field, все поля класса, не зависимо от модификатора доступа,
        // но не вернёт поля классов-родителей
        Field[] fieldsA = aClass.getDeclaredFields();
        System.out.println("\nПЕЧАТАЕМ ПОЛЯ обьекта A");
        for (Field field : fieldsA) {
            System.out.println(field.getName() + " " + field.getType());
        }
        //.getFields(); - возвращает массив типа Field, все публичные поля класса и всех его родителей по цепочке.
        Field[] fieldsB = bClass.getFields();
        System.out.println("\nПЕЧАТАЕМ ПОЛЯ обьекта B");
        for (Field field : fieldsB) {
            System.out.println(field.getName() + " " + field.getType());
        }

        try {
            //.getDeclaredFields("nameMethod"); - возвращает конкретное поле если мы знаем его название
            Field myField = aClass.getDeclaredField("privateFiled");
            System.out.println("\nИМЯ КОНКРЕТНОГО ПОЛЯ \"" + myField.getName() +
                    "\" ЗНАЧЕНИЕ КОНКРЕТНОГО ПОЛЯ \"" + myField.getType() + "\"");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        Method[] methods = aClass.getMethods();
        System.out.println("\nПЕЧАТАЕМ МЕТОДЫ объекта А");//Включая методы классов от которых наследуются, а также Object
        for (Method method : methods) {
            System.out.println(method);
        }

        try {
            //.newInstance(); вызов метода вернет Object, который будет создан по тому самому описанию.
            // К сожалению описанный способ будет работать только с конструктором по умолчанию (без параметров).
            A objA = aClass.newInstance();
            objA.printText();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

//        System.out.println("\nПЕЧАТАЕТ КОНСТРУКТОРЫ");
//        Constructor<?>[] constructors = aClass.getConstructors();
//        for (Constructor<?> constructor : constructors) {
//            System.out.print(constructor.getName() + " (");
//            Parameter[] params = constructor.getParameters();
//            for (Parameter param: params) {
//                System.out.print(param.getType()+ " "+param.getName()+", ");
//            }
//            System.out.println(")");
//        }
    }
}
