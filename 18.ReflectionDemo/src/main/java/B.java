public class B extends A {
    private String BStr;
    public int BInt=0;

     public B(){

     }
    public B(String text, String privateFiled, String BStr, int BInt) {
        super(text, privateFiled);
        this.BStr = BStr;
        this.BInt = BInt;
    }

    public void publicMethodB(){
         System.out.println("I am B class");
     }

     private String printString(){
         System.out.println(BStr);
         return BStr;
     }

    @Override
    public String toString() {
        return "B{" +
                "text='" + text + '\'' +
                ", BStr='" + BStr + '\'' +
                ", BInt=" + BInt +
                '}';
    }
}
