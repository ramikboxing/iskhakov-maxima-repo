public class A {

    public String text;
    private String privateFiled = "privatStr";
    private int privateInt = 100;
    private double privateDouble = 100.5;
    private boolean privateBool = false;

    public A() {
    }


    public A(String text) {
        this.text = text;
    }

    public A(String text, String privateFiled) {
        this.text = text;
    }

    public void printText() {
        System.out.println(text);
        System.out.println("work method printText");
    }

    public String getPrivateFiled() {
        return privateFiled;
    }

    @Override
    public String toString() {
        return "A{" +
                "text='" + text + '\'' +
                ", privateFiled='" + privateFiled + '\'' +
                ", privateInt=" + privateInt +
                ", privateDouble=" + privateDouble +
                ", privateBool=" + privateBool +
                '}';
    }
}
