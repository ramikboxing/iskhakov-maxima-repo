package cars;

import java.io.*;
import java.util.Scanner;

class Main{

	static PrintWriter pw = new PrintWriter(System.out, true);
	static Car [] arrayCars = new Car [10];
	static int size = 0;

	//автозаполнение списка машин
	//1.1 Создаете массив из машин (Car) и заполняете массив данными.
	private void autoWriteArrayCars(){
		String [] arrBrand = {"Subaru", "Toyota", "Honda", "Mazda", "BMW", "Audi","Mersedes", "Opel", "Hundai", "Kia"};
		String [] arrColor = {"Black", "Blue", "White", "Yellow", "Green", "Red", "Grey", "Pink", "Orange"};
		for (int i = 0; i < arrayCars.length; i++ ) {
			Car car = new Car (	i,                        // id
				arrBrand[i],                              // Марка
				arrColor[(int) (Math.random() * 10 - 1)], // Цвет рандомно
				(int) ((Math.random() +1 )*250 ),         // Мощность рандомно	
				(int) ((Math.random()+1 )*10000),         // Пробег
				true,                                     // авто в наличии
				false );                                  // авто не битый	  
			arrayCars[size++] = car;
		}//for
	}

	//печатать список
	//1.2 Нужно вывести все имеющиеся машины:
	private void printArrayCars(Car[] array){
		pw.println("|ID| MODEL |COLOR| POWER | MILLES | STOCK | DAMAGE |");
		for (int i = 0;i < array.length; i++ ) {
			if(array[i] == null){
				continue;
			}
			pw.println(array[i].getId() + " - " 
				+ array[i].getCarBrand() + " - "
				+ array[i].getCarColor() + " - "
				+ array[i].getEnginePower()+ " - "
				+ array[i].getMileage()+ " - "
				+ array[i].inStock()+" - "
				+ array[i].isDamaged());			
		}
		pw.println("\n");
	}

	//Поиск по Id
	//2. Предусмотреть функцию из первого уровня и поиск машины по ID
	private void searchCarById(int id){
		boolean option = true;
		for (int i = 0;i < arrayCars.length; i ++ ) {
			if(id == arrayCars[i].getId()){
				pw.println("Найден: "+ arrayCars[i].getCarBrand()+" - "
					+arrayCars[i].getCarColor()+" - "
					+ arrayCars[i].getEnginePower()
					+"\n");
				option = false;
				// break; т.к. id могут совпасть продолжить поиск
			}
		}//for
			if(option){
				pw.println("Ничего не найдено\n\n");
			}
	}
	//Добавить автомобиль
	//3. Пользователь может добавить машины. При создании новой машины программа должна присвоить уникальный id 
	private Car addNewCarInArray(int id){
		Scanner scanInt = new Scanner(System.in);
		Scanner scanLine = new Scanner(System.in);
		//??? Вопрос. Если использую один сканер вводил id, ввод марки авто пропускается и идет сразу ввод цвета. С чем это связано.
		// Поэтому созданы два сканера  			
		pw.println("Введите данные автомобиля который добавите в список");

		pw.println("Введите марку автомобиля_");
		String carBrand = scanLine.nextLine();
		pw.println("\033[H\033[J");

		pw.println("Введите цвет_");
		String carColor = scanLine.nextLine();
		pw.println("\033[H\033[J");

		pw.println("Какая мощность двигателя_");
		int enginePower = scanInt.nextInt();
		pw.println("\033[H\033[J");

		pw.println("Какой пробег автомобиля");
		int mileage = scanInt.nextInt();	
		pw.println("\033[H\033[J");

		pw.println("Авто в наличии ?   нет,  да");
		String stock = scanLine.nextLine();
		boolean inStock = false;

		if(stock.equals("да") ||stock.equals("yes") ){
			inStock = true;
		}else if(stock.equals("нет") ||stock.equals("no")){
			inStock = false;
		}else{
			pw.println("Ошибка ввода");
			return null;
		}
		pw.println("Авто в битый ?  нет,  да");
		String damage = scanLine.nextLine();
		boolean isDamage = false;

		if(damage.equals("да") || damage.equals("yes") ){
			isDamage = true;
		}else if(damage.equals("нет") ||damage.equals("no")){
			isDamage = false;
		}else{
			pw.println("Ошибка ввода");
			return null;
		}
		Car car = new Car (id, carBrand, carColor, enginePower, mileage, inStock, isDamage);
		return car;
	}

	//обновление массива для авто
	private Car[] newArrayCars(Car [] array){
		Car [] newArrayCars = new Car [array.length + (array.length / 2)];
		for (int i = 0;i < array.length; i++ ) {
			newArrayCars[i] = array[i];
		}
		return newArrayCars;
	}

	//Поиск с фильтрами
	private Car[] fillterCarsArray(int fromEng, int beforeEng, int fromMill,int beforeMilles, int inStock, int isDamage){ 
		Car[] fillteCars = new Car [size];
		int sizefillter = 0;
		for (int i = 0; i < size; i++ ) {
			if((arrayCars[i].getEnginePower() >= fromEng && arrayCars[i].getEnginePower() <= beforeEng) &&
					(arrayCars[i].getMileage() >= fromMill && arrayCars[i].getMileage() <= beforeMilles)){				
				if (inStock == 0 ) { // Авто которых нет в наличии
					if(!arrayCars[i].inStock()){
						fillteCars[sizefillter++] = arrayCars[i];
						continue;
					}
				}else if (inStock == 1){
					if(arrayCars[i].inStock()){
						fillteCars[sizefillter++] = arrayCars[i];
						continue;
					}
				}else if (inStock ==-1){
					fillteCars[sizefillter++] = arrayCars[i];
				}
			}
		}//for
		if(isDamage == -1){
			return fillteCars;			
		}
		Car[] fillterDamageCars = new Car [sizefillter];
		sizefillter = 0;
		for(int i = 0; i < fillterDamageCars.length; i++){
			if(isDamage == 0){
				if(!fillteCars[i].isDamaged()){
					fillterDamageCars[sizefillter++] = fillteCars[i];
					continue;
				}
			}else if (isDamage == 1){
				if(fillteCars[i].isDamaged()){
					fillterDamageCars[sizefillter++] = fillteCars[i];
				}
			}

		}//for

		return fillterDamageCars;
	}

	public static void main(String[] args) {	
		Main mainOb = new Main();
		mainOb.autoWriteArrayCars();
		while (true){
			try{
				Scanner scanner = new Scanner(System.in);
				pw.println("1. печатать список автомобилей");
				pw.println("2. поиск автомобилей");
				pw.println("3. добавить автомобиль в список");
				pw.println("4. поиск фильтром");
				pw.println("9. выход");

				int option = scanner.nextInt();				
				if(option == 1){ //печать списка автомобилей
					if(size == 0){
						pw.println("Нет автомобилей");
						continue;
					}
					mainOb.printArrayCars(arrayCars);

				}else if(option == 2){
					pw.println("Введите Id для поиска_");
					int byId = scanner.nextInt();
					mainOb.searchCarById(byId);
				}else if(option == 3){ //добавление авто
					if(size >= arrayCars.length-1){
						arrayCars = mainOb.newArrayCars(arrayCars);
					}
					Car car = mainOb.addNewCarInArray(size);
					if(car != null){	
						arrayCars[size++] = car;	
					}		
					continue;
				}else if(option == 4){ // поиск по фильтру
					pw.println("Параметры поиска:");

					pw.println("мощность двигателя от _");
					int fromEngine = scanner.nextInt();
					pw.println("мощность двигателя до _");
					int beforeEngine = scanner.nextInt();
					pw.println("\033[H\033[J");

					pw.println("пробег от _");
					int fromMilles = scanner.nextInt();
					pw.println("пробег до _");
					int beforeMilles = scanner.nextInt();
					pw.println("\033[H\033[J");

					pw.println("наличие авто: 1- в наличие: 0- отсутсвует: -1 - пропустить параметр ");
					int inStock = scanner.nextInt(); 
					if (inStock < -1 || inStock > 1 ){
						pw.println("Ошибка ввода параметра \"Наличие авто\"");
						continue;
					}
					pw.println("\033[H\033[J");

					pw.println("ДТП: 1- битый: 0- небитый: -1 - пропустить параметр ");
					int isDamage = scanner.nextInt();
					if(isDamage < -1 || isDamage > 1){
						pw.println("Ошибка ввода параметра \"Состояние авто\"");
						continue;
					} 
					pw.println("\033[H\033[J");
					Car[] carFillter = mainOb.fillterCarsArray(fromEngine, beforeEngine, fromMilles, beforeMilles, inStock, isDamage);
					mainOb.printArrayCars(carFillter);	
					/*Мощность двигателя
                      Пробег
                      Наличие
                      Битый/Не битый*/				
				}else if (option == 9){
					break;
				}
			}catch(Exception e){
				pw.println("Error"+ e);
			}
		}//while
	}//mail
}//Main