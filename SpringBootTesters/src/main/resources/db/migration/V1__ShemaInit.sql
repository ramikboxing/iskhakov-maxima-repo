-- прописываем как заполняется последовательность Id
create sequence customer_id_seq start with 1 increment by 50;
create sequence product_id_seq start with 1 increment by 50;

-- создаём таблицу customers для сущности Customer
create table customers
(
    id    bigint DEFAULT nextval('customer_id_seq') not null,
    name  varchar(255)                              not null,
    email varchar(255)                              not null,
    primary key (id)
);

-- создаём таблицу products для сущности Product
create table products
(
    id          bigint  DEFAULT nextval('customer_id_seq') not null,
    name        varchar(255)                               not null,
    description varchar(255),
    price       numeric                                    not null,
    sold        boolean default false,
    primary key (id)
);
