package com.example.springboottesters.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) // интеграционные тесты, случайный порт
@AutoConfigureMockMvc                                                       // автоматическая конфигурация MockMvc

public class ProductControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;                                                // внедряем бин MockMvc для вызова контроллера

    @Test
    void getAllControllerProductsIntergation() throws Exception {
        // 1. Условия для метода

        // 2. Выполнение метода (проверка функциональности)
        mockMvc.perform(get("/products"))                       // вызываем метод getAllProducts() контроллера

                // 3. Проверка метода
                .andExpect(status().isOk());                              // проверяем статус ответа равен 200 (ОК)
    }

}
