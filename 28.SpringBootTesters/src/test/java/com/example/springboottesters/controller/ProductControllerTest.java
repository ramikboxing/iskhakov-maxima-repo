package com.example.springboottesters.controller;

import com.example.springboottesters.service.ProductService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(controllers = ProductController.class)  // модульный тест для контроллера ProductController.class
class ProductControllerTest {

    @MockBean
    private ProductService productService;              // внедряем бин сервиса, который будем мокать

    @Autowired
    private MockMvc mockMvc;                            // внедряем бин MockMvc для вызова контроллера
    @Test
    void getAllControllerProducts() throws Exception {
        // 1. Условия для метода
        given(productService.getAllProducts())          // метод getAllProducts() возвращает пустой список
                .willReturn(List.of());

        // 2. Выполнение метода (проверка функциональности)
        mockMvc.perform(get("/products"))       // вызываем метод контроллера

                // 3. Проверка метода
                .andExpect(status().isOk());
    }


}