package com.example.springboottesters.repository;
import com.example.springboottesters.domain.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest                                    // аннотация для тестирования репозиториев
class ProductRepositoryTest {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private EntityManager entityManager;        // для записи сущностей в БД используем entityManager

    @Test
    void testFindProductsBySoldIsAndDescriptionIs() {
        // 1. Условия для метода
        Product product1 = Product.builder()
                .id(null)
                .name("p1")
                .description("d1")
                .price(BigDecimal.TEN)
                .sold(false)
                .build();

        Product product2 = Product.builder()
                .id(null)
                .name("p2")
                .description("d2")
                .price(new BigDecimal("10.0"))
                .sold(true)
                .build();


//        productRepository.save(product1);
        entityManager.persist(product1);
        entityManager.persist(product2);

        // 2. Выполнение метода (проверка функциональности)
        List<Product> products = productRepository.findProductsBySoldIsAndDescriptionIs(false, "d1");

        // 3. Проверка метода
        assertThat(products).hasSize(1);
        assertThat(products.get(0).getName()).isEqualTo("p1");
    }
}