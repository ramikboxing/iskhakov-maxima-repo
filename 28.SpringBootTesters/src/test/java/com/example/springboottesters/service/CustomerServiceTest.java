package com.example.springboottesters.service;

import com.example.springboottesters.domain.Customer;
import com.example.springboottesters.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CustomerServiceTest {
    CustomerRepository customerRepository;
    CustomerService customerService;

    @BeforeEach
    void setUp() {
        customerRepository = Mockito.mock(CustomerRepository.class);
        customerService = new CustomerService(customerRepository);
    }

    @Test
    void getAllCustomers() {
        Customer customer1 = Customer.builder()
                .id(10L)
                .name("c1")
                .email("c@mail.ru")
                .build();

        Customer customer2 = Customer.builder()
                .id(20L)
                .name("c2")
                .email("c@mail.ru")
                .build();

        BDDMockito.given(customerRepository.findAll()).willReturn(List.of(customer1, customer2));

        List<Customer>customerList = customerService.getAllCustomers();

        assertThat(customerList).hasSize(2);
        assertThat(customerList.get(0).getId()).isEqualTo(10L);
        assertThat(customerList.get(0).getName()).isEqualTo("c1");
    }
}