package com.example.springboottesters.service;

// в реализации используется Spring Boot context

import com.example.springboottesters.domain.Product;
import com.example.springboottesters.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ProductServiceContextTest {

    @Mock         // productRepository = Mockito.mock(ProductRepository.class); ставится там где нужна заглушка
    private ProductRepository productRepository;
    @InjectMocks  // productService = new ProductService(productRepository); ставится там где нужна проверка
    private ProductService productService;

    @Test
    void getAllUnSoldProducts() {
        // 1. Условия для метода
        Product product1 = Product.builder()
                .id(100L)
                .name("p1")
                .description("d1")
                .price(BigDecimal.TEN)
                .sold(false)
                .build();

        Product product2 = Product.builder()
                .id(200L)
                .name("p2")
                .description("d2")
                .price(new BigDecimal("10.0"))
                .sold(true)
                .build();
        given(productRepository.findAll())
                .willReturn(List.of(product1, product2));

        // 2. Выполнение метода (проверка функциональности)
        List<Product> products = productService.getAllProducts();

        // 3. Проверка метода
        assertThat(products).hasSize(1);
        assertThat(products.get(0).getId()).isEqualTo(100L);
        assertThat(products.get(0).getName()).isEqualTo("p1");
    }
}