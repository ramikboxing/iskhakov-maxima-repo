package com.example.springboottesters.service;

import com.example.springboottesters.domain.Product;
import com.example.springboottesters.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.*;

import static org.junit.jupiter.api.Assertions.*;

class ProductServiceTest {
    private ProductService productService;
    private ProductRepository productRepository;

    @BeforeEach
        //вызывается перед каждым методом с анотацией @Test
    void setUp() {
        productRepository = Mockito.mock(ProductRepository.class); //Заглушка класса ProductRepository
        productService = new ProductService(productRepository);
    }

    @Test
    void getAllProductsUnSoldProducts() {
        //1. Задаем условие для теста
        Product product1 = Product.builder()
                .id(100L)
                .name("p1")
                .description("d1")
                .price(new BigDecimal("10"))
                .sold(false)
                .build();

        Product product2 = Product.builder()
                .id(200L)
                .name("p2")
                .description("d2")
                .price(new BigDecimal("11.5"))
                .sold(true)
                .build();

        BDDMockito.given(productRepository.findAll()).willReturn(List.of(product1, product2));
        //2. Выполнение теста
        List<Product> products = productService.getAllProducts();
        //3. Проверка теста
        assertThat(products).hasSize(1);
        assertThat(products.get(0).getId()).isEqualTo(100L);
        assertThat(products.get(0).getName()).isEqualTo("p1");

    }
}