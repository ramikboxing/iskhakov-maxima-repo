package com.example.springboottesters;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTestersApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTestersApplication.class, args);
    }

}
