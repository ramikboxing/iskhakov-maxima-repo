package com.example.springboottesters.controller;

import com.example.springboottesters.domain.Product;
import com.example.springboottesters.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController                                     // @RestController = @Controller + @ResponseBody
public class ProductController {

    private final ProductService productService;    // внедрение бина сервиса через конструктор

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
        System.out.println(" =============== ProductController =============== ");
    }

    @GetMapping("/products")
    public List<Product> getAllProducts() {
        return productService.getAllProducts();     // вызываем метод сервиса из контроллера
    }
}

