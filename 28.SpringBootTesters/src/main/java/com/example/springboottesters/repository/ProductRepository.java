package com.example.springboottesters.repository;

import com.example.springboottesters.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    @Query("select p from Product p where p.sold = ?1 and p.description = ?2")              // реализация в JPQL
    List<Product> findProductsBySoldIsAndDescriptionIs(Boolean sold, String description);   // наш кастомный метод

}
