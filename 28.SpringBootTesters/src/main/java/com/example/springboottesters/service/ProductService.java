package com.example.springboottesters.service;

import com.example.springboottesters.domain.Product;
import com.example.springboottesters.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import java.util.stream.Collectors;

@Service                                // создаём бин для сервиса
@Transactional                          // операции с методами сервиса выполняются как транзакции
public class ProductService {

    private final ProductRepository productRepository;  // внедрение бина сервиса через конструктор

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository ;
        System.out.println("=============== ProductService =============== ");
    }

    @Transactional(readOnly = true)
    public List<Product> getAllProducts() {
//        List<Product> allById = productRepository.findProductsBySoldIsAndDescriptionIs(false,"d3");
//        List<Product> allById4 = productRepository.findProductsBySoldIsAndDescriptionIs(true,"d4");
        return productRepository.findAll().stream()     // результат выполнения метода репозитория протускаем через стрим
                .filter(product -> !product.isSold())   // фильтруем значения по параметру продан
                .collect(Collectors.toList());          // сохраняем элементы стрима в список
    }

}
