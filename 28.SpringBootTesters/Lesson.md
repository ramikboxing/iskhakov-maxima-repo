# REST API

API 
REST - Representational State Transfer 
«передача репрезентативного состояния» или «передача „самоописываемого“ состояния»

### Restful API
1. Uniform Interface
2. Client-Server
3. Stateless
4. Cacheable
5. Layered System
6. Code on Demand (Optional)

1) Без состояний - всё через отдельные запросы
2) Ориентируемся на ресурсы
3) JSON

POST, PUT, GET, DELETE

GET /users
GET /users/4
GET /users/4/friends
GET /users/4/photos/2

GET /products

POST /users
PUT /users/4

DELETE /users/4/

POST /users/4/friends

GET /users?age=23&date=2022

### Инструменты
* Postman
* [Postman plugin для FireFox](https://addons.mozilla.org/en-US/firefox/addon/restclient/)
* [Postman plugin для Chrome](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=ru)
* **HTTP Client IntelliJ Idea** - предпочтительный вариант

### Документация
* https://habr.com/ru/post/483202/
* https://restfulapi.net/
* https://spring.io/projects/spring-hateoas
* https://spring.io/projects/spring-data-rest
* https://swagger.io/
* https://spring.io/projects/spring-restdocs
* https://dzone.com/articles/developing-rest-apis

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

