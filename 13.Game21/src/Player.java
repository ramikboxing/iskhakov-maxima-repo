import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Player {
    static int id = 1;
    private final String name;
    private int many = 100;

    public String getName() {
        return name;
    }

    public Player() {
        this.name = "player_" + id++;
    }

    public Player(String name) {
        this.name = name;
    }

    public void many(int many) {
        this.many += many;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
