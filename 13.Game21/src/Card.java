public class Card {
    private String cardName;
    private String suit;

    private int point;

    public String getCardName() {
        return cardName;
    }

    public String getSuit() {
        return suit;
    }

    public int getPoint() {
        return point;
    }

    public Card(String cardName, String suit, int point) {
        this.cardName = cardName;
        this.suit = suit;
        this.point = point;
    }
}
