// В данной программе нужно применить HashMap

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите ваше имя");
        String namePlayer = scanner.nextLine();
        Player player = new Player(namePlayer);

        System.out.print("Введите количество игроков_ ");
        int countPlayers = scanner.nextInt();


        Map<Player, Integer> playersPoint = new HashMap<>();
        for (int i = 0; i < countPlayers; i++) {
            Player bot = new Player();
            playersPoint.put(bot, 0);
        }
        playersPoint.put(player, 0);
        // Игороки готовы начинаем игровой процесс
        GamePlayImpl game = new GamePlayImpl();
        game.shuffleTheDeck();

        System.out.println("Обьявляется банк в размере 20 едениц.");
        int bank = 20;
        int bid = bank / 2;
        Iterator<Player> itrPlayersPoint = playersPoint.keySet().iterator();
        while (itrPlayersPoint.hasNext()) {

            Player pl = itrPlayersPoint.next();
            int point = 0;
            if (pl.equals(player)) {
                while (true) {
                    System.out.println("Нажмите любую цифру чтобы взять карту 0 чтобы перстать");
                    int x = scanner.nextInt();
                    if (x == 0) {
                        break;
                    }
                    point = game.thePlayerDrawsACard();
                    playersPoint.put(pl, playersPoint.get(pl) + point);
                }
            } else {
                point = game.thePlayerDrawsACard();
                playersPoint.put(pl, playersPoint.get(pl) + point);

                while (game.isNextCard(playersPoint.get(pl))) {
                    point = game.thePlayerDrawsACard();
                    playersPoint.put(pl, playersPoint.get(pl) + point);
                }
            }
            if (game.isWin(playersPoint.get(pl))) {
                // досрочное завершение
                pl.many(bid);
                System.out.println("Игра завершина победил " + pl.getName() + " набрав "
                        + playersPoint.get(pl) + "  ");
                return;
            }
            if (game.enumeration(playersPoint.get(pl))) {
                pl.many(-bid);
                itrPlayersPoint.remove();
            }
        }//while


        System.out.println("Очки набранные игроками");
        int maxPoint = playersPoint.values().stream().max(Integer::compare).get();
        System.out.println("maxPoint = " + maxPoint);
        Iterator<Player> itrPlayersPoint2 = playersPoint.keySet().iterator();
        while (itrPlayersPoint2.hasNext()) {
            Player pl = itrPlayersPoint2.next();
            System.out.println(pl.getName()+ " " + playersPoint.get(pl));
            if (maxPoint !=playersPoint.get(pl)){
                itrPlayersPoint2.remove();
                continue;
            }
        }
        itrPlayersPoint = playersPoint.keySet().iterator();
        System.out.println("Победил(и)");
        while (itrPlayersPoint.hasNext()) {
            Player playWin = itrPlayersPoint.next();
            System.out.println(playWin.getName());
        }

    }
}
