import java.util.LinkedList;
import java.util.Map;

public class GamePlayImpl implements GamePlay {
    String[] cardName = {"Валет", "Дама", "Король", "Шесть", "Семь", "Восемь", "Девять", "Десять", "Туз"};
    int[] cardPoint = {2, 3, 4, 6, 7, 8, 9, 10, 11};
    String[] suit = {"Черви", "Пики", "Буби", "Крести"};
    LinkedList<Card> cardDesk = new LinkedList<>();

    public GamePlayImpl() {
        createDesk();
    }

    private void createDesk() {
        for (int i = 0; i < cardName.length; i++) {
            for (int j = 0; j < suit.length; j++) {
                Card card = new Card(cardName[i], suit[j], cardPoint[i]);
                cardDesk.add(card);
            }
        }
    }

    @Override
    public void shuffleTheDeck() {
        Card[] cards = new Card[36];
        int count = 0;
        while (count <= 36) {
            int x = (int) (Math.random() * cardDesk.size());
            if (cardDesk.size() == 0) {
                break;
            }
            if (cardDesk.get(x) == null) {
                continue;
            }
            cards[count++] = cardDesk.get(x);
            cardDesk.remove(x);
        }
        for (int i = 0; i < cards.length; i++) {
            cardDesk.add(cards[i]);
        }
    }


    @Override
    public int thePlayerDrawsACard() {
        Card card = cardDesk.pollLast();
        if (card != null) {
            System.out.println(card.getCardName() + " " + card.getSuit());
            return card.getPoint();
        }
        System.out.println("Колода пуста!!!");
        return 0;
    }

    @Override
    public boolean isNextCard(int point) {
        if (point > 10) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isWin(int point) {
        if (point == 21) {
            return true;
        }
        return false;
    }

    @Override
    public boolean enumeration(int point) {
        if (point > 21) {
            System.out.println("Перебор");
            return true;
        }
        return false;
    }

    @Override
    public void finalGame() {


    }

    public void PrintDesk() {
        for (Card card : cardDesk) {
            System.out.println(card.getCardName() + " " + card.getSuit() + " " + card.getPoint());
        }
    }

}
