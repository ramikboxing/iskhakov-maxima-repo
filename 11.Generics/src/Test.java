public class Test {
    private static int index = 0;
    private int id;
    private String name;
    private boolean isLive;

    public static int getIndex() {
        return index;
    }

    public Test(int id, String name, boolean isLive) {
        this.id = id;
        this.name = name;
        this.isLive = isLive;
        index++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isLive() {
        return isLive;
    }

    public void TestPrint() {
        System.out.println("iTest");
    }
}
