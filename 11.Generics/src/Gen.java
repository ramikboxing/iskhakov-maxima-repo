public class Gen<T> implements GenInterface<T> {
    private static final int DEFAULT_SIZE = 10;
    private T[] elements;
    private int size;

    Gen() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    private void resize() {
        T[] oldArray = this.elements;
        this.elements = (T[]) new Object[size * 2];
        for (int i = 0; i < size; i++) {
            this.elements[i] = oldArray[i];
        }
    }

    private boolean isFull() {
        return size == elements.length;
    }

    private void cut() {
        if (size == 0) {
            this.elements = (T[]) new Object[DEFAULT_SIZE];
            return;
        }
        T[] oldArray = this.elements;
        int count = 0;
        this.elements = (T[]) new Object[size * 2];
        for (int i = 0; i < oldArray.length; i++) {
            if (oldArray[i] != null && count < elements.length) {
                elements[count++] = oldArray[i];
            }
        }
    }

    private boolean isSemiEmpty() {
        if ((size * 4) < elements.length) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void add(T element) {
        if (isFull()) {
            resize();
        }
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == null) {
                this.elements[i] = element;
                size++;
                return;
            }
        }
    }

    @Override
    public void add(int index, T element, boolean bool) {
        if (isFull()) {
            resize();
        }
        if (elements[index] == null) {
            elements[index] = element;
            size++;
            return;
        }
        if (bool) {
            elements[index] = element;
            return;
        } else {
            for (int i = elements.length - 1; i > index; ) {
                elements[i] = elements[--i];
            }
            elements[index] = element;
            size++;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) {
        return elements[index];
    }

    @Override
    public void remove(int index) {
        elements[index] = null;
        size--;
        if (isSemiEmpty()) {
            cut();
        }
    }

    @Override
    public int length() {
        return elements.length;
    }

    @Override
    public void print() {
        for (int i = 0; i < elements.length; i++) {
            System.out.println(i + " " + elements[i]);
        }
    }
}
