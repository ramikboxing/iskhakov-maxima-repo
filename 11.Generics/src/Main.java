public class Main {
    public static void main(String[] args) {
        GenInterface<Integer> iGen = new Gen<>();
        // добавление элементов в колекцию
        iGen.add(12);
        iGen.add(13);
        // печать колекции
        iGen.print();
        System.out.println("добавляем в 6 индек разрешая смешение элементов");
        iGen.add(6, 55, false);
        iGen.print();
        iGen.add(6, 56, false);
        iGen.add(6, 57, false);
        System.out.println("добавляем с перезаписью");
        iGen.add(6, 58, true);
        iGen.print();
        // добавляем большое кол-во элементов, что бы вызвать авторасширение
        // колекции
        System.out.println("Размер колекции до авторасширения - " + iGen.size());
        System.out.println("Размер массива до авторасширения - " + iGen.length());
        for (int i = 0; i < 20; i++) {
            iGen.add(777);
        }
        System.out.println("Размер колекции после авторасширения - " + iGen.size());
        System.out.println("Размер массива после авторасширения - " + iGen.length());
        iGen.print();
        // удаляем первые 15 индексов
        for (int i = 0; i < 16; i++) {
            iGen.remove(i);
        }
        System.out.println("Размер колекции после cut - " + iGen.size());
        System.out.println("Размер массива после cut- " + iGen.length());
        iGen.print();

        // Работа с собствеными типами данных
        GenInterface<Test> testCollection = new Gen<>();
        // создадим тестовый тип данных и объекты этого типа
        Test ramil = new Test(Test.getIndex(), "Ramil", true);
        Test ralina = new Test(Test.getIndex(), "Ralina", true);
        Test renita = new Test(Test.getIndex(), "Renita", true);
        Test elza = new Test(Test.getIndex(), "Elza", true);
        Test leo = new Test(Test.getIndex(), "Leo", true);
        // Поместим их в соответствующию колекцию
        testCollection.add(ramil);
        testCollection.add(ralina);
        testCollection.add(4, leo, true);
        testCollection.add(renita);
        testCollection.add(7, elza, true);

        for (int i = 0; i < testCollection.length(); i++) {
            if (testCollection.get(i) != null) {
                System.out.println(testCollection.get(i).getId() + " " +
                        testCollection.get(i).getName());
            }
        }
        System.out.println("Печать всей колекции");
        testCollection.print();
    }
}
