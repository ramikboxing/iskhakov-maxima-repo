public interface GenInterface<T> {

    /**
     * Метод добаляет element в первый свободный индекс
     * после добавления размер (size) колекции увеличивается
     *
     * @param element - элемент для добавления в колекцию
     */
    void add(T element);

    /**
     * Метод помещает element в колекцию под index.
     * Если ячейка колекции свободна, element просто помещается и увеличивается размер (size)
     * колекции.
     * Если ячейка колекции занята, то просматривается параметр bool.
     * Если bool == true, то происходит перезапись ячейки, размер (size) колекции при этом
     * неизменяется
     * Если bool == false, то происходит смещения данных в колекци от index, а на место index записывается
     * element. Размер (size) колекции увеличивается.
     *
     * @param index
     * @param element
     * @param bool
     */
    void add(int index, T element, boolean bool);

    /**
     * Метод возвращает размер колекции
     *
     * @return количество элементов в колекции
     */
    int size();

    /**
     * Метод возвращает элемент из колекции по index
     *
     * @param index
     * @return elements [index]
     */
    T get(int index);

    /**
     * Удаление элемента из колекции по index
     * После удаления размер (size) уменьшается.
     *
     * @param index
     */
    void remove(int index);

    /**
     * Метод возвращает длинну ячеек колекции
     *
     * @return elements.length
     */
    int length();

    /**
     * Печать элементов из колекции
     */
    void print();
}
