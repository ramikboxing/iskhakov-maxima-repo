package reflectionAPI;

import java.lang.reflect.*;
import java.util.*;


public class ReflectionAPI {
    private static Field[] fieldsCreateObj;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите имя класса   reflectionAPI.Person");
        String className = scanner.next();
        Class<?> unknownClass = null;
        try {
            unknownClass = Class.forName(className);
            printConstructors(unknownClass);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //Вывод созданного объекта
        System.out.println(createObject(unknownClass).toString());
    }

    private static int printFields(Class<?> clazz) {
        int length = 0;
        for (Field field : clazz.getDeclaredFields()) {
            System.out.println(field.getType() + " - " + field.getName());
            length++;
        }
        return length;
    }

    private static void printConstructors(Class<?> obj) {
        Constructor<?>[] constructors = obj.getConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.print(constructor.getName() + " (");
            Parameter[] params = constructor.getParameters();
            for (Parameter param : params) {
                System.out.print(param.getType() + " " + param.getName() + ", ");
            }
            System.out.println(")");
        }
    }

    //TODO: реализовать для полей всех типов данных
    private static Object createObject(Class<?> clazz) {
        Scanner scannerCreate = new Scanner(System.in);
        Object object = null;
        Class<?> obj ;
        try {
            obj = Class.forName(clazz.getName());
            Constructor<?>[] constructors = obj.getConstructors();
            Constructor<?> constructor = constructors[constructors.length - 1];
            Parameter[] parameters = constructor.getParameters();
            fieldsCreateObj = obj.getDeclaredFields();
            List<Object> argsList = new ArrayList<>();
            //create ArgsConstructor
            if (parameters.length > 0) {
                for (Parameter parameter : parameters) {
                    if (parameter.getType().getName().equals("java.lang.Integer")) {
                        String argName = getFieldName("java.lang.Integer");
                        System.out.println("Введите " + argName);
                        int argInt ;
                        argInt = scannerCreate.nextInt();
                        argsList.add(argInt);
                    } else if (parameter.getType().getName().equals("java.lang.String")) {
                        String argName = getFieldName("java.lang.String");
                        System.out.println("Введите " + argName);
                        String argStr = scannerCreate.nextLine();
                        argsList.add(argStr);
                    }
                }//for
                Object[] objArray = argsList.toArray();
                object = constructor.newInstance(objArray);
                //NoArgsConstructor
            }else {
                object = constructor.newInstance();
            }
        } catch (ClassNotFoundException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return object;
    }

    private static String getFieldName(String type) {
        String arg = " ";
        for (int j = 0; j < fieldsCreateObj.length; j++) {
            if (fieldsCreateObj[j] != null && fieldsCreateObj[j].getType().getName().equals(type)) {
                arg = fieldsCreateObj[j].getName();
                fieldsCreateObj[j] = null;
                break;
            }
        }
        return arg;
    }
}
