package reflectionAPI;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    private String name;
    private String lastname;
    private String patronymic;
    private Integer age;

//  Раскаментировать  конструктор и закоментировать анотацию @AllArgsConstructor
//  для создания объекта с неполным набором полей

//    public Person(String name, String lastname, Integer age) {
//        this.name = name;
//        this.lastname = lastname;
//        this.age = age;
//    }
}
