package spring;

public class StandardMessageRenderer implements MessageRenderer{

    private Message message;

    public StandardMessageRenderer(Message message) {
        this.message = message;
    }

    @Override
    public void printMessage() {
        System.out.println(message.getText());
    }
}

