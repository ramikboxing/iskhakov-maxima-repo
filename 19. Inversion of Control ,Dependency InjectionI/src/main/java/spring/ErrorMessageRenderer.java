package spring;

public class ErrorMessageRenderer implements MessageRenderer{
    private Message message;

    public ErrorMessageRenderer(Message message) {
        this.message = message;
    }

    @Override
    public void printMessage() {
        System.err.println(message.getText());
    }
}

