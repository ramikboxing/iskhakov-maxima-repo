package spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public Message message() {
        return new HelloMessage("Ramil");
    }

    @Bean
    public Message message2() {
        return new MyMessage("IRamil");
    }

    @Bean
    public MessageRenderer renderer() {
        return new ErrorMessageRenderer(message2());
    }

    @Bean
    public AdvancedMessageRenderer advancedMessageRenderer() {
        return new AdvancedMessageRenderer(renderer());
    }
}
