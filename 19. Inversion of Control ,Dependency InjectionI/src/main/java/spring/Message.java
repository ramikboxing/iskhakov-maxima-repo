package spring;

public interface Message {
    String getText();
}
