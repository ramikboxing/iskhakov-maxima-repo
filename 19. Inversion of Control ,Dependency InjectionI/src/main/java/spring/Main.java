package spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
 //       ApplicationContext context = new ClassPathXmlApplicationContext("config.xml");
//        Message message = (Message) context.getBean("message");
//        System.out.println(message.getText());
//        Message message2 = (Message) context.getBean("message2");
//        System.out.println(message2.getText());
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
//        Message message = (Message) context.getBean("message");
//        System.out.println(message.getText());
//        MessageRenderer renderer = (MessageRenderer) context.getBean("renderer");
//        renderer.printMessage();
        AdvancedMessageRenderer renderer = context.getBean(AdvancedMessageRenderer.class);
        renderer.print();

    }
}
