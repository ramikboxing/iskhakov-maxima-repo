package spring;

public class MyMessage implements Message{
    private String myText;

    public MyMessage(String myText){
        this.myText = "this is my text: "+ myText;
    }
    @Override
    public String getText() {
        return myText;
    }
}
