package spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdvancedMessageRenderer {
    private MessageRenderer messageRenderer;

    @Autowired
    public AdvancedMessageRenderer(MessageRenderer messageRenderer) {
        this.messageRenderer = messageRenderer;
    }
    void print(){
        messageRenderer.printMessage();
    }
}
