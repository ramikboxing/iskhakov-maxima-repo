package dao;

import model.Car;
import model.User;
import model.Wheel;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.*;

public class UserDaoImpl implements UserDao {

    // language=SQL
    private final String SQL_FIND_ALL = "SELECT user_demo.* ,cars.id as car_id," +
            "cars.model as model," +
            "wheel.id as wheel_id," +
            "wheel.radius as radius," +
            "wheel.model as wheel_model " +
            "FROM user_demo LEFT JOIN cars ON user_demo.id = cars.owner_id LEFT JOIN wheel ON cars.id = wheel.car_id";

    private JdbcTemplate template;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    Map<Long, User> userMap = new HashMap<>();
    Map<Long, Car> carsMap = new HashMap<>();
    
    private RowMapper<User> userRowMapper = (ResultSet resultSet, int i) -> {
        boolean nextUser = true;
        Long userId = resultSet.getLong("id");
        if (!userMap.containsKey(userId)) { // Если нет пользователя с таким id, то создать его
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            User user = new User(userId, firstName, lastName, new ArrayList<>());
            userMap.put(userId, user);
            nextUser = false;
        }
        Long carId = resultSet.getLong("car_id");
        boolean nextCar = true;
        if (carId != 0 && !carsMap.containsKey(carId)) { //Если нет  машины с таким id создаем её
            Car car = new Car(
                    resultSet.getLong("car_id"),
                    resultSet.getString("model"),
                    userMap.get(userId),
                    new ArrayList<>());
            carsMap.put(carId, car);
            nextCar = false;
        } else {
            Car car = new Car();
            carsMap.put(0L, car);
        }
        Long wheelId = resultSet.getLong("wheel_id");
        Wheel wheel;
        if (wheelId != 0) {
            wheel = new Wheel(
                    wheelId,
                    resultSet.getInt("radius"),
                    resultSet.getString("wheel_model"),
                    carsMap.get(carId));
        } else {
            wheel = new Wheel();
        }

        User userForCar = userMap.get(userId);
        List<Car> carList = userForCar.getCars();
        if (!nextCar) { //Если авто уже есть в списке, его не добавляем
            carList.add(carsMap.get(carId));
        }
        List<Wheel> wheels = carsMap.get(carId).getWheels();
        if (wheelId != 0) {
            wheels.add(wheel);
        }
        if (nextUser) {
            return null;
        }
        return userMap.get(userId);
    };

    public UserDaoImpl(DataSource dataSource) {
        // Передаем параметры подключения к базе данных
        this.template = new JdbcTemplate(dataSource);
        // Передаем параметры подключения к базе данных
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public void save(User model) {

    }

    @Override
    public Optional<User> find(Long id) {
        return Optional.empty();
    }

    @Override
    public void update(User model) {

    }

    @Override
    public void delete(User model) {

    }

    @Override
    public List<User> findAll() {
        List<User> users = template.query(SQL_FIND_ALL, userRowMapper);
        userMap.clear();
        return users;
    }

    @Override
    public User findById(Long Id) {
        return null;
    }
}

