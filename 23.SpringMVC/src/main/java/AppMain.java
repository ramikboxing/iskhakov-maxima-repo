import dao.UserDao;
import dao.UserDaoImpl;
import model.User;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.List;


public class AppMain {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/postgres",
                "postgres", "481632");
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName("org.postgresql.Driver");
//        dataSource.setUsername("postgres");
//        dataSource.setPassword("481632");
//        dataSource.setUrl("jdbc:postgresql://localhost:5432/spring_user_db");

        UserDao userDao = new UserDaoImpl(dataSource);
        List<User> users = userDao.findAll();
        for (User user: users) {
            if (user != null){
                System.out.println("User__________________________________________________");
                System.out.println(user);
            }
        }
    }
}

