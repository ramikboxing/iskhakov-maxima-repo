package model;

import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = "owner")
public class Car {
    @NonNull
    private Long id;
    @NonNull
    private String model;
    @NonNull
    private  User owner;

    private List<Wheel> wheels;

    @Override
    public String toString() {
        return " \n\tCars {" +
                "\tid=" + id +
                ", model='" + model + '\'' +
                ",\n\t wheels=" + wheels +
                '}';
    }
}

