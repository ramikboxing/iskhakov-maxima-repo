package model;

import lombok.*;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
//@ToString(exclude = "car")
public class Wheel {
    @NonNull
    private Long wheel_id;
    @NonNull
    private Integer radius;
    @NonNull
    private String model;

    private Car car;

    @Override
    public String toString() {
        return "Wheel{" +
                "\n\twheel_id=" + wheel_id +
                ", radius=" + radius +
                ", model='" + model + '\'' +
                '}' + "\n";
    }
}
