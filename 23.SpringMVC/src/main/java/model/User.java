package model;

import lombok.*;
import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    private Long Id;
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;

    private List<Car> cars;

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                "\ncars=" + cars +
                '}';
    }
}

