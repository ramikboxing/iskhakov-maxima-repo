CREATE database "postgres";
create table user_demo (
                           id SERIAL PRIMARY KEY,
                           first_name VARCHAR (30),
                           last_name VARCHAR (30)
);
create table cars(
                     id SERIAL PRIMARY KEY,
                     model VARCHAR(30),
                     owner_id INTEGER REFERENCES user_demo(id)
);

INSERT INTO user_demo(first_name, last_name) VALUES ('Ilon', 'Mask');

CREATE TABLE wheel (id SERIAL PRIMARY KEY,
                    radius INT,
                    model VARCHAR (30),
                    car_id INT);

SELECT user_demo.*, cars.id as car_id, cars.model as model FROM user_demo LEFT JOIN cars ON user_demo.id = cars.owner_id;

INSERT INTO wheel(radius, model, car_id) VALUES (14,'Nokia', 1);
INSERT INTO wheel(radius, model, car_id) VALUES (18,'Pirelli', 3);
INSERT INTO wheel(radius, model, car_id) VALUES (19,'Pirelli', 5);
INSERT INTO wheel(radius, model, car_id) VALUES (15,'Kama', 4);
INSERT INTO wheel(radius, model, car_id) VALUES (17,'Michelin', 2);



SELECT user_demo.*,
       cars.id as car_id,
       cars.model as model,
       wheel.id as wheel_id,
       wheel.radius as radius,
       wheel.model as wheel_model
FROM user_demo LEFT JOIN cars ON user_demo.id = cars.owner_id LEFT JOIN wheel ON cars.id = wheel.car_id ;
